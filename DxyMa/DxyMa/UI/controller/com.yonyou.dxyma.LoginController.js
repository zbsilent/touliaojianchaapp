//JavaScript Framework 2.0 Code
try {
	Type.registerNamespace('com.yonyou.dxyma.LoginController');
	com.yonyou.dxyma.LoginController = function() {
		com.yonyou.dxyma.LoginController.initializeBase(this);
		this.initialize();
	}
	function com$yonyou$dxyma$LoginController$initialize() {
	}

	function com$yonyou$dxyma$LoginController$evaljs(js) {
		eval(js)
	}
	
	var user;

	/**
	 * 登录按钮监听器
	 */
	function com$yonyou$dxyma$LoginController$wloginbutton_onclick(sender, args) {
		user = $id("wusertext").get("value");
		var password = $id("wpasstext").get("value");
		if (user == "") {
			$alert("用户名不能为空");
		} else {
			if (($cache.read("ip") == undefined) && ($cache.read("port") == undefined)) {
				$alert("IP地址和端口号未设置");
				$view.open({
					"viewid" : "com.yonyou.dxyma.Config", //目标页面（首字母大写）全名
					"isKeep" : "true",//打开新页面的同时是否保留当前页面，true为保留，false为不保留
					"callback" : "myCallBack()"				
				})
			} else {
				//连接后台服务器校验账号密码
				var myparam = {
					"actiontype" : "66601",
					"param" : {
						"usercode" : user,
						"password" : password
					}
				};
				//MA服务器ip和端口 从全局变量里面获取
				var ip = $cache.read("ip");
				var port = $cache.read("port");

				$service.writeConfig({
					"host" : ip, //向configure中写入host键值
					"port" : port //向configure中写入port键值
				})
				//$alert(ip);
				//$alert(port);
				$js.showLoadingBar();
				$service.callAction({
					"viewid" : "com.yonyou.mes.HttpCaller", //部署在MA上的Controller的包名
					"action" : "call", //后台Controller的方法名,
					"params" : {
						"myparam" : myparam
					}, //自定义参数，json格式
					"autoDataBinding" : false, //请求回来的数据会在Context中，是否进行数据绑定，默认不绑定
					"contextmapping" : "result", //将返回结果映射到指定的Context字段上，支持fieldName和xx.xxx.fieldName字段全路径，如未指定contextmapping则替换整个Context
					"callback" : "callsuccess()", //请求成功后回调js方法
					"error" : "callerror()"//请求失败回调的js方法
				})
			}
		}
	}

function myCallBack(){
	var ip = $param.getString("ip");
	var port = $param.getString("port");
	$cache.write("ip", ip);
	$cache.write("port", port);
}
	
	function callsuccess() {
		$js.hideLoadingBar();
		var result = $ctx.get("result");
		//$alert(result);
		var jsonobj = JSON.parse(result);
		//$alert(jsonobj);
		var status = jsonobj["statuscode"];
		//$alert(status);
		if (status == 0) {
		var datas = jsonobj["datas"];
			$view.open({
				"viewid" : "com.yonyou.dxyma.Machine", //目标页面（首字母大写）全名
				"isKeep" : "false", //打开新页面的同时是否保留当前页面，true为保留，false为不保留
				"user":datas.cuserid,
				"ip" : $cache.read("ip"),
				"port" : $cache.read("port")
			})
		} else {
			$alert(jsonobj["errinfo"]);
		}
	}


	function callerror() {
		$js.hideLoadingBar();
		$alert("请求失败，请检查IP地址和端口号是否设置正确");
	}

	/**
	 * 跳转到服务器配置界面
	 */
	function com$yonyou$dxyma$LoginController$button1_onclick(sender, args) {
		/*$view.open({
			"viewid" : "com.yonyou.dxyma.Config", //目标页面（首字母大写）全名
			"isKeep" : "false",
			"ip" : $cache.read("ip"),
			"port" : $cache.read("port")
		})*/
		
		$view.open({
			"viewid" : "com.yonyou.dxyma.Config", //目标页面（首字母大写）全名
			"isKeep" : "true",
			"callback" : "myCallBack()",
			
			"ip" : $cache.read("ip"),
			"port" : $cache.read("port")
		})
	}

	/**
	 * 从缓存里读取IP地址和端口号
 * @param {Object} sender
 * @param {Object} args
	 */
	function com$yonyou$dxyma$LoginController$wloginpanel_onload(sender, args) {
		var ip = $param.getString("ip");
		var port = $param.getString("port");
		if (ip != "") {
			$cache.write("ip", ip);
			$cache.write("port", port);
		}
	}

	function com$yonyou$dxyma$LoginController$onkeydown(sender, args) {
		$view.close();
	}


	com.yonyou.dxyma.LoginController.prototype = {
		onkeydown : com$yonyou$dxyma$LoginController$onkeydown,
		wloginpanel_onload : com$yonyou$dxyma$LoginController$wloginpanel_onload,
		button1_onclick : com$yonyou$dxyma$LoginController$button1_onclick,
		wloginbutton_onclick : com$yonyou$dxyma$LoginController$wloginbutton_onclick,
		initialize : com$yonyou$dxyma$LoginController$initialize,
		evaljs : com$yonyou$dxyma$LoginController$evaljs
	};
	com.yonyou.dxyma.LoginController.registerClass('com.yonyou.dxyma.LoginController', UMP.UI.Mvc.Controller);
} catch(e) {
	$e(e);
}
