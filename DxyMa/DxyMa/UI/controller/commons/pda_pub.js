try {


	window.MMPdaPubUtil = {};
	
	//本地当前时间，格式YYYY-MM-DD
	MMPdaPubUtil.getCurrentLocalTime = function() {
		// return (new Date()).format('yyyy-MM-dd');
		return (new Date()).format('yyyy-MM-dd hh:mm:ss');
	};
	
	function fillToSpecialDigit(len, chr, str) {
		var diff = len - str.length;
		if (diff <= 0)
			return str;

		var fillstr = ""
		for (var i = 0; i < diff; i++) {
			fillstr = fillstr + chr;
		}
		str = fillstr + str;
		return str;
	}


	// 获得当前时间
	MMPdaPubUtil.getNowDate = function() {
		var date = new Date();
		var month = date.getMonth() + 1;
		var strDate = date.getDate();
		if (month >= 1 && month <= 9) {
			month = "0" + month;
		}
		if (strDate >= 0 && strDate <= 9) {
			strDate = "0" + strDate;
		}
		var currentdate = date.getFullYear() + "-" + month + "-" + strDate;
		return currentdate;
	}; 

	// 拓展$cache.write方法　缓存参数
	MMPdaPubUtil.putToCache = MMPdaPubUtil.putToCache ||
	function(key, value) {
		$cache.write($ctx.getApp("username") + '_' + key, value);
	};
	
	MMPdaPubUtil.readFromCache = MMPdaPubUtil.readFromCache ||
	function(key) {
		return $cache.read($ctx.getApp("username") + '_' + key);
	};

	// 拼URL
	MMPdaPubUtil.getURL = MMPdaPubUtil.getURL ||
	function(servicename) {
		var host = $service.readConfig("host");
		var port = $service.readConfig("port");
		return "http://" + host + ":" + port + "/service/" + servicename;
		// return "http://" + $service.readConfig("host") + ":" + $service.readConfig("port") + "/service/" + servicename;
	};

	// $service.post请求
	MMPdaPubUtil.servicePost = function(url, data, callback, error,userTag) {
		userTag = userTag ||'';
		$app.ajax({
			"url" : url, // 请求url地址
			'method':'post',
			'type':'json',
			'tag':userTag,
			"data" : data, // 请求参数
			"callback" : callback, // 请求成功后回调js方法
			"error" : error, // 异常回调
			'header' : {
				'Content-Type' : 'application/json'
			},
			"timeout" : 15000
		})
	};
	
	
	
MMPdaPubUtil.wrapperReport = function(report)
{
		var row = report;
		if(!row)
		{
			return null;
		}
		var that = this;
		var item = {};
		item['tplanstarttime'] 	= row['tplanstarttime'];
		item['tplanendtime'] 	= row['tplanendtime'];
		item['measrate'] 		= row['measrate'] || row['item_measrate'];
		item['cbatchid'] 		= row['cbatchid'] || row['item_cbatchid'] ;
		item['vbillcode'] 		= row['vbillcode'] || row['item_vbillcode'];
		item['nplannum'] 		= row['nplannum'] || row['item_nplannum'];
		item['nplanastnum'] 	= row['nplanastnum'] || row['item_nplanastnum'];
		item['cassmeasureid'] 	= row['cassmeasureid'] || row['item_cmeasureid'] || item['head_cassmeasureid'];
		item['cinvcode'] 		= row['cinvid'] || row['item_cinvid'] || row['head_cinvid'];
		item['vrowno'] 			= row['vrowno'] || row['item_vrowno'];
		item['pk_mo'] 			= row['pk_mo'] || row['item_pk_mo'] || row['head_pk_mo'];
		item['pk_mo_b'] 		= row['pk_mo_b'] || row['item_pk_mo_b'] || row['head_pk_mo_b'];
		item['pk_mo_bid'] 		= row['pk_mo_b']  || row['item_pk_mo_b'] || row['item_pk_mo_bid'];
		item['pk_moid'] 		= row['pk_moid'] || row['item_pk_moid'];
		item['nwrnum'] 			= row['nwrnum']    || row['item_nwrnum'] || -1 ;
		item['nwrassnum'] 		= row['nwrassnum'] || row['item_nwrassnum'] || -1;
		item['vserialcode'] 	= row['vserialcode'] || row['item_vserialcode'];
		
		delete item['cbatchid'];
		
		item = that.validaterForFilter(item);
		
		return item;
	};
	
/**
 * 校验并过滤数组
 */
MMPdaPubUtil.validaterForFilter =  function(item)
{
	var billitem = item || {};
	var propertyNames = Object.getOwnPropertyNames(billitem) || [];
	Array.prototype.forEach.call(propertyNames,function(name,i,arr){
		
		if(!billitem[name] && billitem.hasOwnProperty(name) && (typeof(billitem[name])!='number'))
		{
			delete billitem[name];
		}
		else if(!!billitem[name] && typeof(billitem[name])=='string')
		{
			if(billitem[name].trim().length==0)
			{
				delete billitem[name];
			}
		}
		else if(!!billitem[name] && typeof(billitem[name])=='number')
		{
			if(billitem[name]==NaN || isNaN(billitem[name]))
			{
				delete billitem[name];
			}
			
		}else{
			delete billitem[name];
		}
	});
	
	return billitem;
};
/**
 * 计算换算后的扫描数量
 * @param {Object} item
 */
MMPdaPubUtil.computeNwrassnum = function(item)
{
	
	if(!item)
	{
		return NaN;
	}
	var that = this;
	item['nwrnum'] = item['nwrnum'] || item['item_nwrnum'] || item['head_nwrnum'];
	item['cassmeasureid'] = item['cassmeasureid'] || item['item_cassmeasureid'] || item['head_cassmeasureid'];
	if(!item['nwrnum'] || isNaN(item['nwrnum']))
	{
		return NaN;
	}
	
	if(!item['cassmeasureid'])
	{
		return NaN;
	}
	var bitNumber = that.getBitNumber(item['cassmeasureid']);
	
	var nwrnum = Number(item['nwrnum']);
	console.log('----->nwrnum='+nwrnum+","+'bitNumber='+bitNumber);
	item['measrate'] = item['measrate'] || item['item_measrate'] || '';
	console.log('measrate='+item['measrate']);
	
	item['measrate'] = item['measrate'].replace('\/','/');
	var measrate =  item['measrate'];
	console.log('--<measrate>-----'+measrate+'------------<measrate>--');
	if(!!measrate && measrate.length>0)
	{
		var measrateArray = measrate.split('/') || [];
		if(Array.isArray(measrateArray) && measrateArray.length>0)
		{
				if(measrateArray.length==2)
				{
					var R = Number(measrateArray[1]);
					var S = Number(measrateArray[0]);
					var rate = Number(R/S);
					console.log('--<rate>-----'+rate+'------------<rate>--');
					item['nwrassnum'] = nwrnum * rate;
				}
				if(!isNaN(item['nwrassnum']) && bitNumber!=-1)
				{
					return Number(item['nwrassnum']).toFixed(bitNumber);
				}else{
					return  Number(item['nwrassnum']);
				}
			}
		}
		return NaN;
};


/**
 * 合并查询并且合并bbitem by vserialcode array
 */
MMPdaPubUtil.queryBBItems = function(bbitems)
{
	if(!Array.isArray(bbitems) || bbitems.length<=0)
	{
		return [];
	}
	var tableName 		= SerialNOVO.prototype.getTableName();
	var vserialcodeAttr = SerialNOVO.VSERIALCODE;
	var sql = "SELECT * FROM " +tableName+" WHERE 1=1 ";
		sql = sql +" AND " + vserialcodeAttr.name+" IN (";
	if(vserialcodeAttr.type==MMPdaDataUtil.VALUE_TYPE_TEXT)
	{
		bbitems = Array.prototype.map.call(bbitems,function(item,i,arr){
			return "'"+item+"'";
		});
	}
	sql = sql + bbitems.join(',');
	sql = sql +") ";
	
	bbitems.removeAll();
	MMPdaDataUtil.queryBySql(sql,function(data){
		var dataset = data || [];
		dataset = Array.prototype.map.call(dataset,function(sd){
			var dtoSD = {};
			dtoSD['pk_serialno'] = sd['pk_serial'];
			dtoSD['vserialno'] 	 = sd['vserialcode'];
			return {bbitem:dtoSD};
		});
		Array.prototype.push.apply(bbitems,dataset);
	});
	return bbitems;
}

/**
 * 加载档案，防治多次加载 
 *
 *  @param {Function} _VO_CLASS_ VO类型
 *  @param {Boolean} isForceload是否强制加载
 */
MMPdaPubUtil.loadProfile = function(_VO_CLASS_,isForceload)
{
	var tableName = _VO_CLASS_.prototype.getTableName();
	var prefix = "_VO_CLASS_";
	var cache_key = prefix+tableName;
	
	if(!isForceload &&!!tableName &&  Array.isArray(window[cache_key]))
	{
		return window[prefix+tableName];
	}

	var sqlWhere = " WHERE 1=1 ";
	var dataset = [];
	MMPdaDataUtil.queryVO(_VO_CLASS_,sqlWhere,function(data){
		var datalist = data || [];
		if(Array.isArray(datalist) && datalist.length>0)
		{
			if(typeof(datalist[0])=='object')
			{
				dataset = datalist;
				window[cache_key] = datalist;
			}else{
				window[cache_key] = null; //这里设置为null，表示不能重新加载缓存
			}
		}else{
			window[cache_key] = datalist;
		}
	});
	return dataset;
}
/**
 *获取有效位 
 * @param {Object} cassmeasureid
 */
MMPdaPubUtil.getBitNumber = function(cassmeasureid)
{
	
	if(!cassmeasureid)
	{
		console.log('cassmeasureid = '+cassmeasureid);
		return -1;
	}

	var that = this;
	
	var profiles = that.loadProfile(MeasdocVO,false) || [];
	var attrProfilePK = MeasdocVO.prototype.getPrimaryAttr();
	var adapterProfiles = Array.prototype.filter.call(profiles,function(profile){
		return profile[attrProfilePK.name] == cassmeasureid;
	});	
	var logInfo = {adapterProfiles:adapterProfiles};
	console.log($jsonToString(logInfo));
	
	var bitNumber = -1;
	if(Array.isArray(adapterProfiles) && adapterProfiles.length>0)
	{
		bitNumber = Number(adapterProfiles[0][MeasdocVO.BITNUMBER.name]);
	}
	
	return bitNumber;
}

MMPdaPubUtil.queryVONoCallback = function(VOClass, wheresql) {
	if(MMPdaPubUtil.isTableExist(VOClass) == false)
		return null;
	
	var tblname = VOClass.prototype.getTableName();
	var cols = VOClass.prototype.getAllAttrs();
	if (wheresql == null) {
		wheresql = "";
	}
	var sql = "select * from " + tblname + " " + wheresql;
	var param = {
		db : MMPdaDataUtil.DatabaseName,
		sql : sql,
		pageIndex : 0,
		pageSize : MMPdaDataUtil.PageSize
	}
	var data = $sqlite.queryByPage(param);
	return data;
};

	MMPdaPubUtil.isEmpty = function(value) {
		if(value == null)
			return true;
		if(typeof(value) == "undefined")
			return true;
		if(value == "")
			return true;
		if(value instanceof Array && value.length == 0)
			return true;
		return false;
	};
	MMPdaPubUtil.isNotEmpty = function(value) {
		return !MMPdaPubUtil.isEmpty(value);
	};
/**
 * 判断表是否存在
 * @param {Object} VOClass
 */
MMPdaPubUtil.isTableExist = function(VOClass) {
	var sql = "select count(*)  count from sqlite_master where type='table' and name='" + VOClass.prototype.getTableName() + "'";
	var param = {
		db: MMPdaDataUtil.DatabaseName,
		sql: sql,
		pageIndex: 0,
		pageSize: MMPdaDataUtil.PageSize
	}
	var data = $sqlite.queryByPage(param);
	data = eval(data);
	data = parseInt(data[0]["count"]);
	if(data > 0)
		return true;
	else
		return false;
};

MMPdaPubUtil.formatSQLValue = function(type,value)
{
	if(type==MMPdaDataUtil.VALUE_TYPE_TEXT)
	{
		return "'"+value+"'";
	}else{
		return value;
	}
};

MMPdaPubUtil.fixPrefixProperty = function(row)
{
	if(typeof(row)!='object')
	{
		return row;
	}
	var dataBox =  row || {};
	for(var name in row)
	{
		dataBox[name] = dataBox[name] || row[name];
		if(!dataBox[name])
		{
			if((name.indexOf('item_')==0 || name.indexOf('head_')==0))
			{
				dataBox[name]  = dataBox[name.substring(5)]= row[name.substring(5)];
			}else{
				var value = row['item_'+name]||row['head_'+name];
				dataBox[name]  = dataBox['item_'+name]= value;
			}
		}else{
			if((name.indexOf('item_')==0 || name.indexOf('head_')==0))
			{
				dataBox[name.substring(5)] = dataBox[name];
			}else{
				dataBox['item_'+name]= dataBox[name];
			}
		}
	}
	return dataBox;
};
MMPdaPubUtil.sum = function(values)
{
	var  v = 0;
	for(var i=0;i<values.length;i++)
	{
		v = v + values[i];
	}
	
	return v;
};
MMPdaPubUtil.showMenuAtView = function(menu,view)
{
	$menu.openDropDownList({
           "controlid" :view,
           "dropDownListWidth" : "100",
	        "background" : "#ffffff",
	        "font-size" : "14",
	        "halign" : "center",
	        "border-color" : "#d2d2d2",
	        "split-color" : "#d2d2d2",
	        "showtype":"right",
           "dropItemsArray" : menu
     });
};


} catch(e) {
	$e(e);
}
