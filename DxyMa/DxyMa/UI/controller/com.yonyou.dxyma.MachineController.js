//JavaScript Framework 2.0 Code
try {
	Type.registerNamespace('com.yonyou.dxyma.MachineController');
	com.yonyou.dxyma.MachineController = function() {
		com.yonyou.dxyma.MachineController.initializeBase(this);
		this.initialize();
	}
	var mac;
	
	function com$yonyou$dxyma$MachineController$initialize() {
	}

	function com$yonyou$dxyma$MachineController$evaljs(js) {
		eval(js)
	}
	var bool;
	function com$yonyou$dxyma$MachineController$pageOnLoad(sender, args) {
		bool = false;
		//获取机台名称
		var myparam = {
			"actiontype" : "66602",
			"param" : {
				"wctype" : "涂压"
			}
		};
		//MA服务器ip和端口 从全局变量里面获取
		if($param.getString("ip")==""&&$param.getString("port")==""&&$param.getString("user")==""){
			//$alert("down");
			var ip = $cache.read("ip");
			var port = $cache.read("port");
			var user = $cache.read("user");
		}else{
			//$alert("up");
			var ip = $param.getString("ip");
			var port = $param.getString("port");
			var user = $param.getString("user");
			$cache.write("ip",ip);
			$cache.write("port",port);
			$cache.write("user",user);
		}
		$service.writeConfig({
            "host" : ip,//向configure中写入host键值
            "port" : port //向configure中写入port键值
        })
		$service.callAction({
            "viewid" : "com.yonyou.mes.HttpCaller",//部署在MA上的Controller的包名
            "action" : "call",//后台Controller的方法名,
            "params" : {"myparam":myparam},//自定义参数，json格式
            "autoDataBinding" : false,//请求回来的数据会在Context中，是否进行数据绑定，默认不绑定
            "contextmapping" : "result",//将返回结果映射到指定的Context字段上，支持fieldName和xx.xxx.fieldName字段全路径，如未指定contextmapping则替换整个Context
            "callback" : "callsuccess()",//请求成功后回调js方法
            "error" : "callerror()"//请求失败回调的js方法
        })
	}
	var result;
	function callsuccess(){
		result = $ctx.get("result");
		//$alert(result);
		var jsonobj = JSON.parse(result);
		//$alert(jsonobj);
		var status = jsonobj["statuscode"];
		//$alert(status);
		if (status == 0) {
			var datas = jsonobj["datas"];
			//$alert(datas);
			var context = {
				machine_codes : []
			};
			
			if(datas.length>0){
				bool=true;
				for (var i = 0; i <= datas.length - 1; i++) {
					var data = datas[i];
					context.machine_codes[i] = data.name;
				}
				$ctx.push(context);
				if(datas[0].pk!=null){
					mac = datas[0].name;
					machine_pk = datas[0].pk;
				}
			}else{
				$alert("没有机台");
			}
			
		} else {
			alert("check datas");
		}
	}
	
	function callerror(){
		$alert("获取机台名称失败，请检查IP地址和端口号");
	}
	
	function onChange() {
		//var result = $ctx.get("result");
		//$alert(result);
		
		var jsonobj = JSON.parse(result);
		//$alert(jsonobj);
		var datas = jsonobj["datas"];
		//$alert("编号:" + $ctx.getString("machine_code"));
		var macs = $ctx.getString("machine_code");
		//$alert(macs);
		for(var i = 0; i <= datas.length - 1; i++){
			if(macs == datas[i].name){
				break;
			}
		}
		machine_pk = datas[i].pk;
		mac = macs;
	}
var machine_pk;
	function com$yonyou$dxyma$MachineController$button2_onclick(sender, args) {
		//$alert(mac);
		var data = {
			"mac": mac,
			"pk" : machine_pk		
		}
		//$alert(machine_pk);
		if(bool==true){
			$view.open({
				"viewid" : "com.yonyou.dxyma.Infos", //目标页面（首字母大写）全名
				"isKeep" : "true",
				"data" : data,
				"user":$cache.read("user"),
				"ip":$param.getString("ip"),
	       		"port":$param.getString("port"),
	       		"callback":"mycallback()",
	       		"animation-direction" : "left",
	       		"animation-time" : "1000", //动画持续时间，以毫秒为单位
	    		"animation-type" : "Push" //动画持续时间，以毫秒为单位
			})
		}else{
			$alert("当前没有机台，不能登陆");
		}
		

	}

function mycallback(){
	
}

function com$yonyou$dxyma$MachineController$picker0_onload(sender, args){

}	com.yonyou.dxyma.MachineController.prototype = {
    picker0_onload : com$yonyou$dxyma$MachineController$picker0_onload,
		button2_onclick : com$yonyou$dxyma$MachineController$button2_onclick,
		pageOnLoad : com$yonyou$dxyma$MachineController$pageOnLoad,
		initialize : com$yonyou$dxyma$MachineController$initialize,
		evaljs : com$yonyou$dxyma$MachineController$evaljs
	};
	com.yonyou.dxyma.MachineController.registerClass('com.yonyou.dxyma.MachineController', UMP.UI.Mvc.Controller);
} catch(e) {
	$e(e);
}
