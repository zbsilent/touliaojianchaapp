//JavaScript Framework 2.0 Code
try {
	Type.registerNamespace('com.yonyou.dxyma.InfosController');
	var contextdaras ;
	var machine_pk;
	var machine_name;
	var select_data;
	var select_id;
	var bool = false;
	
	com.yonyou.dxyma.InfosController = function() {
		com.yonyou.dxyma.InfosController.initializeBase(this);
		this.initialize();
	}
	function com$yonyou$dxyma$InfosController$initialize() {
	}

	function com$yonyou$dxyma$InfosController$evaljs(js) {
		eval(js)
	}
	function com$yonyou$dxyma$InfosController$viewPage0_onload(sender, args) {
	//$alert($param.getJSONObject("data"));
		machine_name = $param.getJSONObject("data").mac;
		machine_pk = $param.getJSONObject("data").pk;
		//$alert(machine_pk);
		$id("label7").set("value",machine_name);
		var myparam = {
			"actiontype" : "66604",
			"param" : {
				"wcid" : machine_pk
			}
		};
		//MA服务器ip和端口 从全局变量里面获取
		if($param.getString("ip")==""&&$param.getString("port")==""&&$param.getString("user")==""){
			var ip = $cache.read("ip");
			var port = $cache.read("port");
			var user = $cache.read("user");
		}else{
			var ip = $param.getString("ip");
			var port = $param.getString("port");
			var user = $param.getString("user");
			$cache.write("ip",ip);
			$cache.write("port",port);
			$cache.write("user",user);
		}
		$service.writeConfig({
            "host" : ip,//向configure中写入host键值
            "port" : port //向configure中写入port键值
        })
		$service.callAction({
            "viewid" : "com.yonyou.mes.HttpCaller",//部署在MA上的Controller的包名
            "action" : "call",//后台Controller的方法名,
            "params" : {"myparam":myparam},//自定义参数，json格式
            "autoDataBinding" : false,//请求回来的数据会在Context中，是否进行数据绑定，默认不绑定
            "contextmapping" : "result",//将返回结果映射到指定的Context字段上，支持fieldName和xx.xxx.fieldName字段全路径，如未指定contextmapping则替换整个Context
            "callback" : "callsuccess()",//请求成功后回调js方法
            "error" : "callerror()"//请求失败回调的js方法
        })
	}
	function callsuccess(){
		var result = $ctx.get("result");
		//$alert(result);
		var jsonobj = JSON.parse(result);
		//$alert(jsonobj);
		var status = jsonobj["statuscode"];
		//$alert(status);
		if (status == 0) {
			var datas = jsonobj["datas"];
			//$alert(datas);
			if(datas==null){
				$alert("此机台没有待投料任务");
				$view.close();
			}else{
				var newDatas = [];
				for (var i = 0; i <= datas.length - 1; i++) {
					var data = datas[i];
					newDatas[i] = {};//实例化
					
					newDatas[i].pk_machineorder = data.pk_machineorder;
					newDatas[i].vbillcode = data.vbillcode;
					newDatas[i].material_name = data.material.name;
					newDatas[i].material_code = data.material.code;
					newDatas[i].material_type = data.material.type;
					newDatas[i].order_state = data.orderstate.name;
					newDatas[i].material_pk = data.material.pk_material;
					newDatas[i].material_weight = data.ndemainnum;
					newDatas[i].symbol = "";
				}
				contextdaras = {
					"datas" : newDatas
				};
				if(contextdaras!=null){
					contextdaras["datas"][0]["symbol"] ="*";
				}
				
				$ctx.push(contextdaras);
				bool  = true;
				}
			
		} else {
		
			$alert("此机台没有待投料任务");
			$view.close();
					}
	}
	
	function callerror(){
		$alert("error");	
	}
	 
function com$yonyou$dxyma$InfosController$listviewdefine0_onload(sender, args){
		$ctx.push(contextdaras);
}
function com$yonyou$dxyma$InfosController$onItemClick(sender, args){
  
	 var row = $id("listviewdefine0").get("rowindex");	  
	 for(var i = 0; i<= contextdaras["datas"].length-1;i++){
	   if(row == i&&contextdaras["datas"][i]["symbol"] == ""){
	   		contextdaras["datas"][i]["symbol"] = "*";
	   }else{
	   		contextdaras["datas"][i]["symbol"] = "";
	   } 
	 }	 
	 $ctx.push(contextdaras);
}
//点击按钮把数据传递给下个界面
function com$yonyou$dxyma$InfosController$onQueryButClick(sender, args){
     //$alert(contextdaras["datas"]);
     var data;
     if(bool==true){
	     for(var i = 0; i<= contextdaras["datas"].length-1;i++){
		   if(contextdaras["datas"][i]["symbol"] =="*"){
		        data = contextdaras["datas"][i];
		        break;
		   }
		 }
		 select_data = data;
		 select_id = i;
		 if(i==contextdaras["datas"].length){
		 	$alert("未选中物料项，请选择");
		 }else if(contextdaras["datas"][i]["order_state"]=="下达"){
		 	if($confirm("当前选中的指令未生产，是否确定扫码投料？")){
		 		var datas = {
		 			"mac":machine_name,
		 			"pk":machine_pk,
		 	 		"data":data
		 		} 
	     		$view.open({
	        		"viewid" : "com.yonyou.dxyma.Scan",//目标页面（首字母大写）全名
	        		"isKeep" : "false",
	        		"user":$cache.read("user"),
	        		"ip" : $cache.read("ip"),
	        		"port" : $cache.read("port"),
	        		"data":datas,
	        		"callback" : "myCallBack()"//关闭viewid页面后，执行的回调方法
	    		})
		 	}else{
		 		$Toast("重新选择物料");
		 	}
		 }else{
		 	var datas = {
		 	 	"mac":machine_name,
		 		"pk":machine_pk,
		 	 	"data":data
		 	} 
		 	//var str = JSON.stringify(datas);
		 	$view.open({
	        	"viewid" : "com.yonyou.dxyma.Scan",//目标页面（首字母大写）全名
	        	"isKeep" : "false",
	        	"user":$cache.read("user"),
	        	"ip" : $cache.read("ip"),
	        	"port" : $cache.read("port"),
	        	"data":datas,
	        	"callback" : "myCallBack()"//关闭viewid页面后，执行的回调方法
	    	})
		 }	
     }else{
     	$alert("数据未加载成功，请重新点击");
     	
     }
	 
}
function myCallBack(){
	var num = $param.getString("num");
	//$alert(num);
	select_data.material_weight = (select_data.material_weight-num).toFixed(1);
	contextdaras["datas"][select_id]=select_data;
	$ctx.push(contextdaras);
}
function com$yonyou$dxyma$InfosController$onkeydown(sender, args){
    $view.close({
        "resultCode": "0",//当resultCode为15时，可以响应父页面的callback
    })
}
com.yonyou.dxyma.InfosController.prototype = {
    onkeydown : com$yonyou$dxyma$InfosController$onkeydown,
    onQueryButClick : com$yonyou$dxyma$InfosController$onQueryButClick,
    onItemClick : com$yonyou$dxyma$InfosController$onItemClick,
    listviewdefine0_onload : com$yonyou$dxyma$InfosController$listviewdefine0_onload,
	viewPage0_onload : com$yonyou$dxyma$InfosController$viewPage0_onload,
	initialize : com$yonyou$dxyma$InfosController$initialize,
	evaljs : com$yonyou$dxyma$InfosController$evaljs
	};
	com.yonyou.dxyma.InfosController.registerClass('com.yonyou.dxyma.InfosController', UMP.UI.Mvc.Controller);
} catch(e) {
	$e(e);
}
