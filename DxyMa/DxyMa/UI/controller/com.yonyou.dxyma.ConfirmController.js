//JavaScript Framework 2.0 Code
try {
	Type.registerNamespace('com.yonyou.dxyma.ConfirmController');
	com.yonyou.dxyma.ConfirmController = function() {
		com.yonyou.dxyma.ConfirmController.initializeBase(this);
		this.initialize();
	}
	function com$yonyou$dxyma$ConfirmController$initialize() {
	}

	function com$yonyou$dxyma$ConfirmController$evaljs(js) {
		eval(js)
	}
var data;
var material_pk;
var pk_machineorder;
var scanParmater;
var material_code ;
var lphcode ;
var jhcode ;
var weight  ;
var jt_code ;
var machine_pk ;
var scanmaterial_name;
var scanmaterial_type;
var strs;
var zl_code;
var vbillcode;
var bool;
var ispp;
	//界面打开后加载数据
	function com$yonyou$dxyma$ConfirmController$pageOnLoad(sender, args) {
		bool=true;
		ispp= false;
		// 加载机台物料信息
		if ($param.getString("ip") == "" && $param.getString("port") == ""&&$param.getString("user")=="") {
			var ip = $cache.read("ip");
			var port = $cache.read("port");
			var user = $cache.read("user");
		} else {
			var ip = $param.getString("ip");
			var port = $param.getString("port");
			var user = $param.getString("user");
			$cache.write("ip", ip);
			$cache.write("port", port);
			$cache.write("user",user);
		}
		data = $param.getJSONObject("data");
		//$alert(str);
		jt_code = data["mac"];
		machine_pk = data["pk"]
		var parmater = data["data"];
		vbillcode = parmater["vbillcode"];
		zl_code = parmater["material_code"];
		var cp_name = parmater["material_name"];
		var material_type = parmater["material_type"]
		material_pk = parmater["material_pk"];
		pk_machineorder = parmater["pk_machineorder"];
		$id("jtlabel").set("value",jt_code);
		$id("zllabel").set("value",vbillcode);
		$id("cplabel").set("value",cp_name+"["+material_type+"]");
		var scanString = $param.getString("scannerData");
		strs =  scanString.split(";");
		material_code = strs[1];
		lphcode = strs[2];
		jhcode = strs[3];
		weight = strs[4];
		var myparam1 = {
			"actiontype" : "66605",
			"param" : {
				"material_pk" : material_code
			}
		};
		$service.writeConfig({
            "host" : $cache.read("ip"),//向configure中写入host键值
            "port" : $cache.read("port") //向configure中写入port键值
        })
		$service.callAction({
            "viewid" : "com.yonyou.mes.HttpCaller",//部署在MA上的Controller的包名
            "action" : "call",//后台Controller的方法名,
            "params" : {"myparam":myparam1},//自定义参数，json格式
            "autoDataBinding" : false,//请求回来的数据会在Context中，是否进行数据绑定，默认不绑定
            "contextmapping" : "result1",//将返回结果映射到指定的Context字段上，支持fieldName和xx.xxx.fieldName字段全路径，如未指定contextmapping则替换整个Context
            "callback" : "callsuccess1()",//请求成功后回调js方法
            "error" : "callerror1()"//请求失败回调的js方法
        })
        
        
		 //material = scanString.substring(0,20).replace(/@/g,"");
		 //lphcode = scanString.substring(20,40).replace(/@/g,"");
		 //jhcode = scanString.substring(40,60).replace(/@/g,"");
		 //weight  = scanString.substring(60,80).replace(/@/g,"");
		
	}
	function callsuccess1(){
		var result = $ctx.get("result1");
			//$alert(result);
		var jsonobj = JSON.parse(result);
		//$alert(jsonobj);
		var status = jsonobj["statuscode"];
			//$alert(status);
		if (status == 0) {
				var datas = jsonobj["datas"];
				if(datas==null){
					$alert("物料码不正确，请检查，重新扫码");
					
				}else{
					scanmaterial_name = datas[0].material_name;
					scanmaterial_type = datas[0].material_type;
					
				}
		}
		try{
			//scanParmater = JSON.parse(scanString);
			//var regu =/^.{79}@$/;
			//var re = new RegExp(regu);
			if(strs.length==5){
				if(material_code == zl_code){
						$id("gzlabel").set("value",scanmaterial_name+"["+scanmaterial_type+"]");
						$id("lphlabel").set("value",lphcode);
						$id("jhlabel").set("value",jhcode);
						$id("weightlabel").set("value",weight);
						$id("label1").set("value","匹配");
						$id("label1").set("background","#00FF00");
						$id("button2").set("value","确认投料");
						
					}else{
						$id("gzlabel").set("value",scanmaterial_name+"["+scanmaterial_type+"]");
						$id("lphlabel").set("value",lphcode);
						$id("jhlabel").set("value",jhcode);
						$id("weightlabel").set("value",weight);
					}
			}else{
				$alert("二维码信息错误，请检查");
			}
			
		}catch(err){
			$alert("二维码信息错误，请检查");
		}
		ispp = true;
	}
function com$yonyou$dxyma$ConfirmController$button2_onclick(sender, args){
	if(ispp==true){
		var parmater = data["data"];
		if($id("label1").get("value")=="匹配"){
			var myparam = {
				"actiontype" : "66603",
				"param" : {
					"pk_machineorder" : pk_machineorder,
					"pk_material" : material_pk,
					"nums" : weight,
					"batchcode" : lphcode,
					"user":$cache.read("user")
				}
			};
			$service.writeConfig({
	            "host" : $cache.read("ip"),//向configure中写入host键值
	            "port" : $cache.read("port") //向configure中写入port键值
	        })
			if(bool==true){
					$service.callAction({
		            "viewid" : "com.yonyou.mes.HttpCaller",//部署在MA上的Controller的包名
		            "action" : "call",//后台Controller的方法名,
		            "params" : {"myparam":myparam},//自定义参数，json格式
		            "autoDataBinding" : false,//请求回来的数据会在Context中，是否进行数据绑定，默认不绑定
		            "contextmapping" : "result",//将返回结果映射到指定的Context字段上，支持fieldName和xx.xxx.fieldName字段全路径，如未指定contextmapping则替换整个Context
		            "callback" : "callsuccess()",//请求成功后回调js方法
		            "error" : "callerror()"//请求失败回调的js方法
		        });
		        bool = false;
			}else{
				$alert("请不要重复点击投料")
			}
			
		}else{
			$view.open({
	        	"viewid" : "com.yonyou.dxyma.Scan",//目标页面（首字母大写）全名
	        	"isKeep" : "false",
	        	"user":$cache.read("user"),
	        	"ip" : $cache.read("ip"),
	        	"port" : $cache.read("port"),
	        	"data": data
	    	})
		}
	}else{
		$alert("数据未加载成功，请重新点击");
	}
}

function callerror1(){
	$alert("物料解析失败，请检查");
}
function callsuccess(){
	var result = $ctx.get("result");
	//$alert(result);
	var jsonobj = JSON.parse(result);
	//$alert(jsonobj);
	var status = jsonobj["statuscode"];
	//$alert(status);
	if(status==0){
		$alert("投料成功");
		
		var data_in = {
			"mac": jt_code,
			"pk" : machine_pk		
		}
		$view.open({
			"viewid" : "com.yonyou.dxyma.Infos", //目标页面（首字母大写）全名
			"isKeep" : "false",
			"data" : data_in,
			"user":$cache.read("user"),
			"ip":$cache.read("ip"),
       		"port":$cache.read("port"),
       		
		});
		/*var machine = {
			"mac" : data["mac"],
			"pk" : data["pk"] 
		}
		$view.open({
        	"viewid" : "com.yonyou.dxyma.Infos",//目标页面（首字母大写）全名
        	"isKeep" : "false",
        	"data": machine
    	})*/
    	//$view.close({
        //    "resultCode": "15",//当resultCode为15时，可以响应父页面的callback
        //    "num": weight//返回结果，result1为自定义键值
        //});
	}else{
		$alert(jsonobj["errinfo"]);
		$alert("投料失败，请重新投料");
	}
}

function callerror(){
	$alert("投料请求失败，请检查网络连接");
}
function com$yonyou$dxyma$ConfirmController$button3_onclick(sender, args){
	var data_info = {
			"mac": jt_code,
			"pk" : machine_pk		
		}
	
	$view.open({
			"viewid" : "com.yonyou.dxyma.Infos", //目标页面（首字母大写）全名
			"isKeep" : "false",
			"data" : data_info,
			"user":$cache.read("user"),
			"ip":$cache.read("ip"),
       		"port":$cache.read("port"),
       		
		});

}
function com$yonyou$dxyma$ConfirmController$onkeydown(sender, args){
	var data_inf = {
			"mac": jt_code,
			"pk" : machine_pk		
		}
	
	$view.open({
			"viewid" : "com.yonyou.dxyma.Infos", //目标页面（首字母大写）全名
			"isKeep" : "false",
			"data" : data_inf,
			"user":$cache.read("user"),
			"ip":$cache.read("ip"),
       		"port":$cache.read("port"),
       		
		});
}	
com.yonyou.dxyma.ConfirmController.prototype = {
    onkeydown : com$yonyou$dxyma$ConfirmController$onkeydown,
    button2_onclick : com$yonyou$dxyma$ConfirmController$button2_onclick,
    button3_onclick : com$yonyou$dxyma$ConfirmController$button3_onclick,
		pageOnLoad : com$yonyou$dxyma$ConfirmController$pageOnLoad,
		initialize : com$yonyou$dxyma$ConfirmController$initialize,
		evaljs : com$yonyou$dxyma$ConfirmController$evaljs
	};
	
	com.yonyou.dxyma.ConfirmController.registerClass('com.yonyou.dxyma.ConfirmController', UMP.UI.Mvc.Controller);
} catch(e) {
	$e(e);
}
function show() {
	alert("绑定字段的值：" + $id("wtogglebuttongroup0").getAttribute("selectedValue"));
}
