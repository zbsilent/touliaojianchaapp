<?xml version="1.0" encoding="UTF-8"?>

<window xmlns:web="http://www.yonyou.com/uapmobile/dsl" canvaswidth="375" controller="LoginController" orientation="vertical" canvasheight="667" namespace="com.yonyou.dxyma" id="Login" onkeydown="this.onkeydown()">
    <import ref="Login.css" type="css"/>
    <link href="sys/theme.css" type="text/css"/>
    <div id="viewPage0">
        <navigatorbar id="navigatorbar0" title="登陆界面" class="navigatorbarclass">
            <input id="button0" type="button" class="buttonclass"/>
            <label id="label0"/>
            <input onclick="this.button1_onclick()" id="button1" type="button" class="buttonclass"/> 
        </navigatorbar>
        <div id="wloginpanel" onload="this.wloginpanel_onload()">
            <div id="wuserpanel">
                <image src="fa_user.png" id="wuserimg" scaletype="fitcenter"/>
                <input maxlength="256" id="wusertext" placeholder="手机\\用户名\\邮箱" type="text"/> 
            </div>
            <div id="wpasspanel">
                <image src="fa_password.png" id="wpassimg" scaletype="fitcenter"/>
                <input maxlength="256" id="wpasstext" placeholder="密码" type="password"/> 
            </div>
            <div id="wforgetpasspanel"/>
            <input onclick="this.wloginbutton_onclick()" id="wloginbutton" type="button" value="登录" class="buttonclass loginbuttonclass"/>
            <div id="wotherpanel"/> 
        </div> 
    </div> 
</window>
