<?xml version="1.0" encoding="UTF-8"?>

<window xmlns:web="http://www.yonyou.com/uapmobile/dsl" controller="InfosController" namespace="com.yonyou.dxyma" id="Infos" onkeydown="this.onkeydown()">  
    <import ref="Infos.css" type="css"/>  
    <link href="sys/theme.css" type="text/css"/>  
    <script src="#{path.controller}/commons/AppService.js" type="text/javascript"/>  
    <div id="viewPage0" onload="this.viewPage0_onload()"> 
        <div id="panel3"> 
            <div id="panel4"> 
                <navigatorbar id="navigatorbar0" title="机台指令" class="navigatorbarclass"> 
                    <label id="label11"/>  
                    <label id="label7"/> 
                </navigatorbar> 
            </div>  
            <div id="panel0"> 
                <label id="label2">物料</label>  
                <label id="label12">产地</label>  
                <label id="label3">待投料量</label>  
                <label id="label8">指令状态</label>  
                <label id="label6">选中</label> 
            </div>  
            <listView bindfield="datas" collapsed="true" id="listviewdefine0" onitemclick="this.onItemClick()"> 
                <div id="panel1"> 
                    <label bindfield="material_name" id="label4">label</label>  
                    <label bindfield="material_type" id="label13">label</label>  
                    <label bindfield="material_weight" id="label5">label</label>  
                    <label bindfield="order_state" id="label9">label</label>  
                    <label bindfield="symbol" id="label0">label</label> 
                </div> 
            </listView> 
        </div>  
        <div id="panel2"> 
            <input imagebuttontype="icon" istogglebutton="false" onclick="this.onQueryButClick()" checked="false" id="imagebutton0" type="imagebutton" value="检查" class="imagebuttonclass"/> 
        </div> 
    </div> 
</window>
