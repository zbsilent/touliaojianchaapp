<?xml version="1.0" encoding="UTF-8"?>

<window xmlns:web="http://www.yonyou.com/uapmobile/dsl" controller="ConfirmController" namespace="com.yonyou.dxyma" id="Confirm" onkeydown="this.onkeydown()">  
    <import ref="Confirm.css" type="css"/>  
    <link href="sys/theme.css" type="text/css"/>  
    <div id="viewPage0" onload="this.pageOnLoad()"> 
        <navigatorbar id="navigatorbar0" title="投料信息" class="navigatorbarclass"> 
            <label id="label0"/> 
        </navigatorbar>  
        <div id="wpanel6"> 
            <div id="wpanel0"> 
                <label id="wlabel0">机台名称:</label>  
                <label id="jtlabel"/> 
            </div>  
            <div id="wpanel1"> 
                <label id="wlabel1">指令号:</label>  
                <label id="zllabel"/> 
            </div>  
            <div id="wpanel3"> 
                <label id="wlabel3">产品名称:</label>  
                <label id="cplabel"/> 
            </div>  
            <div id="panel1"/>  
            <div id="wpanel5"> 
                <label id="wlabel5">物料信息</label> 
            </div>  
            <div id="wpane22"> 
                <label id="wlabe22">物料:</label>  
                <label id="gzlabel"/> 
            </div>  
            <div id="wpane23"> 
                <label id="wlabe23">炉批号:</label>  
                <label id="lphlabel"/> 
            </div>  
            <div id="wpane24"> 
                <label id="wlabe24">卷号:</label>  
                <label id="jhlabel"/> 
            </div>  
            <div id="wpane25"> 
                <label id="wlabe25">重量:</label>  
                <label id="weightlabel"/> 
            </div>  
            <div id="wpanel9"> 
                <label id="wlabel9">匹配结果:</label>  
                <label id="label1">不匹配</label> 
            </div>  
            <div id="panel0"> 
                <input onclick="this.button2_onclick()" id="button2" type="button" value="重新扫码" class="textbtnclass"/>  
                <input onclick="this.button3_onclick()" id="button3" type="button" value="取消投料" class="textbtnclass"/> 
            </div> 
        </div> 
    </div> 
</window>
