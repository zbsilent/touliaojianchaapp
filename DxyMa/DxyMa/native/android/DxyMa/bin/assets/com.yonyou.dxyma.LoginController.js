//JavaScript Framework 2.0 Code
try{
Type.registerNamespace('com.yonyou.dxyma.LoginController');
com.yonyou.dxyma.LoginController = function() {
    com.yonyou.dxyma.LoginController.initializeBase(this);
    this.initialize();
}
function com$yonyou$dxyma$LoginController$initialize(){
    //you can programing by $ctx API
    //get the context data through $ctx.get()
    //set the context data through $ctx.push(json)
    //set the field of the context through $ctx.put(fieldName, fieldValue)
    //get the parameter of the context through $ctx.param(parameterName)
    //Demo Code:
    //    var str = $ctx.getString();      //获取当前Context对应的字符串
    //    alert($ctx.getString())          //alert当前Context对应的字符串
    //    var json = $ctx.getJSONObject(); //获取当前Context，返回值为json
    //    json["x"] = "a";        //为当前json增加字段
    //    json["y"] = [];           //为当前json增加数组
    //    $ctx.push(json);            //设置context，并自动调用数据绑定
    //    
    //    put方法需手动调用databind()
    //    var x = $ctx.get("x");    //获取x字段值
    //    $ctx.put("x", "b");     //设置x字段值
    //    $ctx.put("x", "b");     //设置x字段值
    //    $ctx.databind();            //调用数据绑定才能将修改的字段绑定到控件上
    //    var p1 = $param.getString("p1");   //获取参数p2的值，返回一个字符串
    //    var p2 = $param.getJSONObject("p2");   //获取参数p3的值，返回一个JSON对象
    //    var p3 = $param.getJSONArray("p3");   //获取参数p1的值，返回一个数组
    
    //your initialize code below...
    
}
    
function com$yonyou$dxyma$LoginController$evaljs(js){
    eval(js)
}
function com$yonyou$dxyma$LoginController$wloginbutton_onclick(sender, args){
	var user = $id("wusertext").get("value");
	var password = $id("wpasstext").get("value");
	if(user==""){
		$alert("用户名不能为空");
	}else{
		if(($cache.read("ip")==undefined)&&($cache.read("port")==undefined)){
			$alert("IP地址和端口号未设置");
			$view.open({
                "viewid" : "com.yonyou.dxyma.Config",//目标页面（首字母大写）全名
                "isKeep" : "false"//打开新页面的同时是否保留当前页面，true为保留，false为不保留
            })
		}else{
			var isSuccess = "true";
			if(isSuccess == "true"){
				$view.open({
                	"viewid" : "com.yonyou.dxyma.Machine",//目标页面（首字母大写）全名
                	"isKeep" : "false",//打开新页面的同时是否保留当前页面，true为保留，false为不保留
                	"ip":$cache.read("ip"),
       				"port":$cache.read("port")
            	})
			}else{
				$alert("用户名和密码错误，请重新输入");
			}
		}
	}
}
function com$yonyou$dxyma$LoginController$button1_onclick(sender, args){
	$view.open({
       "viewid" : "com.yonyou.dxyma.Config",//目标页面（首字母大写）全名
       "isKeep" : "false",
       "ip":$cache.read("ip"),
       "port":$cache.read("port")
    })
}
function com$yonyou$dxyma$LoginController$wloginpanel_onload(sender, args){
	var ip = $param.getString("ip");
	var port = $param.getString("port");
	if(ip!=""){
		$cache.write("ip", ip);
		$cache.write("port",port);
	}
}
function com$yonyou$dxyma$LoginController$onkeydown(sender, args){
	$view.close();
}
com.yonyou.dxyma.LoginController.prototype = {
    onkeydown : com$yonyou$dxyma$LoginController$onkeydown,
    wloginpanel_onload : com$yonyou$dxyma$LoginController$wloginpanel_onload,
    button1_onclick : com$yonyou$dxyma$LoginController$button1_onclick,
    wloginbutton_onclick : com$yonyou$dxyma$LoginController$wloginbutton_onclick,
    initialize : com$yonyou$dxyma$LoginController$initialize,
    evaljs : com$yonyou$dxyma$LoginController$evaljs
};
com.yonyou.dxyma.LoginController.registerClass('com.yonyou.dxyma.LoginController',UMP.UI.Mvc.Controller);
}catch(e){$e(e);}
