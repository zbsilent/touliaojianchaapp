//JavaScript Framework 2.0 Code
try{
Type.registerNamespace('com.yonyou.dxyma.ConfigController');
com.yonyou.dxyma.ConfigController = function() {
    com.yonyou.dxyma.ConfigController.initializeBase(this);
    this.initialize();
}
function com$yonyou$dxyma$ConfigController$initialize(){
    //you can programing by $ctx API
    //get the context data through $ctx.get()
    //set the context data through $ctx.push(json)
    //set the field of the context through $ctx.put(fieldName, fieldValue)
    //get the parameter of the context through $ctx.param(parameterName)
    //Demo Code:
    //    var str = $ctx.getString();      //获取当前Context对应的字符串
    //    alert($ctx.getString())          //alert当前Context对应的字符串
    //    var json = $ctx.getJSONObject(); //获取当前Context，返回值为json
    //    json["x"] = "a";        //为当前json增加字段
    //    json["y"] = [];           //为当前json增加数组
    //    $ctx.push(json);            //设置context，并自动调用数据绑定
    //    
    //    put方法需手动调用databind()
    //    var x = $ctx.get("x");    //获取x字段值
    //    $ctx.put("x", "b");     //设置x字段值
    //    $ctx.put("x", "b");     //设置x字段值
    //    $ctx.databind();            //调用数据绑定才能将修改的字段绑定到控件上
    //    var p1 = $param.getString("p1");   //获取参数p2的值，返回一个字符串
    //    var p2 = $param.getJSONObject("p2");   //获取参数p3的值，返回一个JSON对象
    //    var p3 = $param.getJSONArray("p3");   //获取参数p1的值，返回一个数组
    
    //your initialize code below...
    
}
    
function com$yonyou$dxyma$ConfigController$evaljs(js){
    eval(js)
}
function com$yonyou$dxyma$ConfigController$onRegButClick(sender, args){
	var ip = $id("textbox0").get("value");
   	var port = $id("textbox1").get("value");
    	
    if(ip == ""){
    $alert("请填写IP值");
    }else{
    	$alert("注册成功");
    	$view.open({
       		"viewid" : "com.yonyou.dxyma.Login",//目标页面（首字母大写）全名
        	"isKeep" : "false",
        	"ip" : ip,
        	"port": port
        	//"callback" : "myCallBack()"//关闭viewid页面后，执行的回调方法
    	})
    }
}
function com$yonyou$dxyma$ConfigController$windowOnLoad(sender, args){
	//调用外部接口
	//有返回值 则 设置button
	$id("imagebutton0").set("checked", "true") //设置选中
	//无返回值 则 设置button 为未选中
	$id("imagebutton0").set("checked", "false") //设置未选中
	$window.show
}
function com$yonyou$dxyma$ConfigController$pageOnLoad(sender, args){
	
}
function com$yonyou$dxyma$ConfigController$panel1_onload(sender, args){
	if($param.getString("ip")!=""){
		$id("textbox0").set("value",$param.getString("ip"));
		$id("textbox1").set("value",$param.getString("port"));
	}else{
		$id("textbox1").set("placeholder","请输入端口号");
		$id("textbox0").set("placeholder","请输入ip地址");
	}
}
function com$yonyou$dxyma$ConfigController$onkeydown(sender, args){
	$view.open({
       	"viewid" : "com.yonyou.dxyma.Login",//目标页面（首字母大写）全名
        "isKeep" : "false",
        "ip" : "",
        "port": ""
        //"callback" : "myCallBack()"//关闭viewid页面后，执行的回调方法
    })
}
com.yonyou.dxyma.ConfigController.prototype = {
    onkeydown : com$yonyou$dxyma$ConfigController$onkeydown,
    panel1_onload : com$yonyou$dxyma$ConfigController$panel1_onload,
    pageOnLoad : com$yonyou$dxyma$ConfigController$pageOnLoad,
    windowOnLoad : com$yonyou$dxyma$ConfigController$windowOnLoad,
    onRegButClick : com$yonyou$dxyma$ConfigController$onRegButClick,
    initialize : com$yonyou$dxyma$ConfigController$initialize,
    evaljs : com$yonyou$dxyma$ConfigController$evaljs
};
com.yonyou.dxyma.ConfigController.registerClass('com.yonyou.dxyma.ConfigController',UMP.UI.Mvc.Controller);
}catch(e){$e(e);}
