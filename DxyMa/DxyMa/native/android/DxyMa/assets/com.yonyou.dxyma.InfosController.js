//JavaScript Framework 2.0 Code
try {
	Type.registerNamespace('com.yonyou.dxyma.InfosController');
	var contextdaras ;
	var wk_id;
	com.yonyou.dxyma.InfosController = function() {
		com.yonyou.dxyma.InfosController.initializeBase(this);
		this.initialize();
	}
	function com$yonyou$dxyma$InfosController$initialize() {
		//you can programing by $ctx API
		//get the context data through $ctx.get()
		//set the context data through $ctx.push(json)
		//set the field of the context through $ctx.put(fieldName, fieldValue)
		//get the parameter of the context through $ctx.param(parameterName)
		//Demo Code:
		//    var str = $ctx.getString();      //获取当前Context对应的字符串
		//    alert($ctx.getString())          //alert当前Context对应的字符串
		//    var json = $ctx.getJSONObject(); //获取当前Context，返回值为json
		//    json["x"] = "a";        //为当前json增加字段
		//    json["y"] = [];           //为当前json增加数组
		//    $ctx.push(json);            //设置context，并自动调用数据绑定
		//
		//    put方法需手动调用databind()
		//    var x = $ctx.get("x");    //获取x字段值
		//    $ctx.put("x", "b");     //设置x字段值
		//    $ctx.put("x", "b");     //设置x字段值
		//    $ctx.databind();            //调用数据绑定才能将修改的字段绑定到控件上
		//    var p1 = $param.getString("p1");   //获取参数p2的值，返回一个字符串
		//    var p2 = $param.getJSONObject("p2");   //获取参数p3的值，返回一个JSON对象
		//    var p3 = $param.getJSONArray("p3");   //获取参数p1的值，返回一个数组

		//your initialize code below...
		//var wk_id = $param.getString("wk_id");

	}

	function createXMLHttpRequest() {
		var xmlHttp;
		if (window.XMLHttpRequest) {
			xmlHttp = new XMLHttpRequest();
			if (xmlHttp.overrideMimeType)
				xmlHttp.overrideMimeType('text/xml');
		} else if (window.ActiveXObject) {
			try {
				xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try {
					xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e) {
				}
			}
		}
		return xmlHttp;
	}

	function com$yonyou$dxyma$InfosController$evaljs(js) {
		eval(js)
	}

	function com$yonyou$dxyma$InfosController$button0_onclick(sender, args) {

	}

	function com$yonyou$dxyma$InfosController$onitemclick(sender, args) {
		//当前选中行
		var data = $id("listviewdefine0").get("row");
		var vs = data["material_code"]

	}
	var list_data;
	var next_data;
	function com$yonyou$dxyma$InfosController$viewPage0_onload(sender, args) {
	//$alert($param.getJSONObject("data"));
		wk_id = $param.getString("data");
		//var wk_id = "wewew";
		$id("label7").set("value",wk_id);
		var myparam = {
			"actiontype" : "66602",
			"param" : {
				"wcid" : "1001E41000000000UJKJ"
			}
		};
		//MA服务器ip和端口 从全局变量里面获取
		if($param.getString("ip")==""&&$param.getString("port")==""){
			//$alert("down");
			var ip = $cache.read("ip");
			var port = $cache.read("port");
		}else{
			//$alert("up");
			var ip = $param.getString("ip");
			var port = $param.getString("port");
			$cache.write("ip",ip);
			$cache.write("port",port);
		}
		$service.writeConfig({
            "host" : ip,//向configure中写入host键值
            "port" : port //向configure中写入port键值
        })
		$service.callAction({
            "viewid" : "com.yonyou.mes.HttpCaller",//部署在MA上的Controller的包名
            "action" : "call",//后台Controller的方法名,
            "params" : {"myparam":myparam},//自定义参数，json格式
            "autoDataBinding" : false,//请求回来的数据会在Context中，是否进行数据绑定，默认不绑定
            "contextmapping" : "result",//将返回结果映射到指定的Context字段上，支持fieldName和xx.xxx.fieldName字段全路径，如未指定contextmapping则替换整个Context
            "callback" : "callsuccess()",//请求成功后回调js方法
            "error" : "callerror()"//请求失败回调的js方法
        })
	}
	function callsuccess(){
		var result = $ctx.get("result");
		//$alert(result);
		var jsonobj = JSON.parse(result);
		//$alert(jsonobj);
		var status = jsonobj["statuscode"];
		//$alert(status);
		if (status == 0) {
			var datas = jsonobj["datas"];
			//$alert(datas);
			var newDatas = [];
			for (var i = 0; i <= datas.length - 1; i++) {
				var data = datas[i];
				//$alert(data);
				//$alert(data.materia);
				//$alert(data.material.name);
				newDatas[i] = {};//实例化
				
				newDatas[i].material_code = data.material.name;
				//$alert(data.nnum + data.material.dw);
				newDatas[i].material_num = data.nnum + data.material.dw;
				newDatas[i].symbol = "";
			}
			contextdaras = {
				"datas" : newDatas
			};
			list_data = contextdaras;
			$ctx.push(contextdaras);
		} else {
			alert("check datas");
		}
	}
	function callerror(){
		//$alert("error");
		var no = $param.getString("no");
		var newDatas = [];
		var listData = [];
		if(wk_id=="粗拉机台#01"||wk_id=="粗拉机台#02"||wk_id=="粗拉机台#03"){
			if(no==""){
				$cache.write("weight_0", 52);
			}
			newDatas[0]={};
			newDatas[0].material_order = "CL00061";
			newDatas[0].material_name = "焊丝CHW-50C(桶)";
			newDatas[0].no = "0";
			newDatas[0].symbol = "";
			
			listData[0]={};
			listData[0].material_weight = $cache.read("weight_0")+"吨";
			listData[0].order_state = "生产中";
			listData[0].material_name = "焊丝CHW-50C(桶)";
			listData[0].symbol = "";
			
			if(no==""){
				$cache.write("weight_1", 40);
			}
			newDatas[1]={};
			newDatas[1].material_order = "CL000102";
			newDatas[1].material_name = "盘条ER70S-6";
			newDatas[1].no = "1";
			newDatas[1].symbol = "";
			
			listData[1]={};
			listData[1].material_weight = $cache.read("weight_1")+"吨";
			listData[1].order_state = "下单";
			listData[1].material_name = "盘条ER70S-6";
			listData[1].symbol = "";
		}else{
			if(no==""){
				$cache.write("weight_0", 39);
			}
			newDatas[0]={};
			newDatas[0].material_order = "CL000121";
			newDatas[0].material_name = "焊丝CHW-50C(桶)";
			newDatas[0].no = "0";
			newDatas[0].symbol = "";
			
			listData[0]={};
			listData[0].material_weight = $cache.read("weight_0")+"吨";
			listData[0].order_state = "生产中";
			listData[0].material_name = "焊丝CHW-50C(桶)";
			listData[0].symbol = "";
			
			if(no==""){
				$cache.write("weight_1", 60);
			}
			newDatas[1]={};
			newDatas[1].material_order = "CL000136";
			newDatas[1].material_name = "盘条ER70S-6";
			newDatas[1].no = "1";
			newDatas[1].symbol = "";
			
			listData[1]={};
			listData[1].material_weight = $cache.read("weight_1")+"吨";
			listData[1].order_state = "下单";
			listData[1].material_name = "盘条ER70S-6";
			listData[1].symbol = "";
		}
		if(no!=""){
			listData[no].material_weight = $cache.read("weight_"+no)-$param.getString("num")+"吨";
			$cache.write("weight_"+no,$cache.read("weight_"+no)-$param.getString("num"));
		}
		contextdaras = {
			"datas" : listData
		};
		contextdaras_1 = {
			"datas" : newDatas
		};
		list_data = contextdaras;
		next_data = contextdaras_1
		$ctx.push(contextdaras);
	}
	 



function com$yonyou$dxyma$InfosController$listviewdefine0_onload(sender, args){
		$ctx.push(list_data);
}
//退出按钮操作
function com$yonyou$dxyma$InfosController$imagebutton1_onclick(sender, args){
	$view.close();
}
function com$yonyou$dxyma$InfosController$onItemClick(sender, args){
  
	 var row = $id("listviewdefine0").get("rowindex");	  
	 for(var i = 0; i<= contextdaras["datas"].length-1;i++){
	   if(row == i){
	   		contextdaras["datas"][i]["symbol"] = "*";
	   		
	   }else{
	   		contextdaras["datas"][i]["symbol"] = "";
	   } 
	 }	 
	 $ctx.push(contextdaras);
}
//点击按钮把数据传递给下个界面
function com$yonyou$dxyma$InfosController$onQueryButClick(sender, args){
     //$alert(contextdaras["datas"]);
     var data;
     // 
	 for(var i = 0; i<= contextdaras["datas"].length-1;i++){
	   
	   if(contextdaras["datas"][i]["symbol"] =="*"){
	        data = contextdaras_1["datas"][i];
	        break;
	   }
	   
	 }
	 if(i==contextdaras["datas"].length){
	 	$alert("未选中物料项，请选择");
	 }else if(contextdaras["datas"][i]["order_state"]=="下单"){
	 	if($confirm("当前选中的指令未生产，是否确定扫码投料？")){
	 		var datas = {
	 			"jt_code":wk_id,
	 	 		"data":data
	 		} 
     		$view.open({
        		"viewid" : "com.yonyou.dxyma.Scan",//目标页面（首字母大写）全名
        		"isKeep" : "false",
        		"data":datas,
        		//"callback" : "myCallBack()"//关闭viewid页面后，执行的回调方法
    		})
	 	}else{
	 		$Toast("重新选择物料");
	 	}
	 }else{
	 	var datas = {
	 		"jt_code":wk_id,
	 	 	"data":data
	 	} 
	 	//var str = JSON.stringify(datas);
	 	$view.open({
        	"viewid" : "com.yonyou.dxyma.Scan",//目标页面（首字母大写）全名
        	"isKeep" : "false",
        	"data":datas,
        	//"callback" : "myCallBack()"//关闭viewid页面后，执行的回调方法
    	})
	 }	
}
function com$yonyou$dxyma$InfosController$onkeydown(sender, args){
	$view.open({
         "viewid" : "com.yonyou.dxyma.Machine",//目标页面（首字母大写）全名
         "isKeep" : "false"//打开新页面的同时是否保留当前页面，true为保留，false为不保留
    })
}
com.yonyou.dxyma.InfosController.prototype = {
    onkeydown : com$yonyou$dxyma$InfosController$onkeydown,
    onQueryButClick : com$yonyou$dxyma$InfosController$onQueryButClick,
    onItemClick : com$yonyou$dxyma$InfosController$onItemClick,
    imagebutton1_onclick : com$yonyou$dxyma$InfosController$imagebutton1_onclick,
    listviewdefine0_onload : com$yonyou$dxyma$InfosController$listviewdefine0_onload,
		viewPage0_onload : com$yonyou$dxyma$InfosController$viewPage0_onload,
		onitemclick : com$yonyou$dxyma$InfosController$onitemclick,
		button0_onclick : com$yonyou$dxyma$InfosController$button0_onclick,
		initialize : com$yonyou$dxyma$InfosController$initialize,
		evaljs : com$yonyou$dxyma$InfosController$evaljs
	};
	com.yonyou.dxyma.InfosController.registerClass('com.yonyou.dxyma.InfosController', UMP.UI.Mvc.Controller);
} catch(e) {
	$e(e);
}
