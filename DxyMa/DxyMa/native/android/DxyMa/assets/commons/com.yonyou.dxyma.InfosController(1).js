//JavaScript Framework 2.0 Code
try {
	Type.registerNamespace('com.yonyou.dxyma.InfosController');
	com.yonyou.dxyma.InfosController = function() {
		com.yonyou.dxyma.InfosController.initializeBase(this);
		this.initialize();
	}
	function com$yonyou$dxyma$InfosController$initialize() {
		//you can programing by $ctx API
		//get the context data through $ctx.get()
		//set the context data through $ctx.push(json)
		//set the field of the context through $ctx.put(fieldName, fieldValue)
		//get the parameter of the context through $ctx.param(parameterName)
		//Demo Code:
		//    var str = $ctx.getString();      //获取当前Context对应的字符串
		//    alert($ctx.getString())          //alert当前Context对应的字符串
		//    var json = $ctx.getJSONObject(); //获取当前Context，返回值为json
		//    json["x"] = "a";        //为当前json增加字段
		//    json["y"] = [];           //为当前json增加数组
		//    $ctx.push(json);            //设置context，并自动调用数据绑定
		//
		//    put方法需手动调用databind()
		//    var x = $ctx.get("x");    //获取x字段值
		//    $ctx.put("x", "b");     //设置x字段值
		//    $ctx.put("x", "b");     //设置x字段值
		//    $ctx.databind();            //调用数据绑定才能将修改的字段绑定到控件上
		//    var p1 = $param.getString("p1");   //获取参数p2的值，返回一个字符串
		//    var p2 = $param.getJSONObject("p2");   //获取参数p3的值，返回一个JSON对象
		//    var p3 = $param.getJSONArray("p3");   //获取参数p1的值，返回一个数组

		//your initialize code below...
		//var wk_id = $param.getString("wk_id");

	}

	function createXMLHttpRequest() {
		var xmlHttp;
		if (window.XMLHttpRequest) {
			xmlHttp = new XMLHttpRequest();
			if (xmlHttp.overrideMimeType)
				xmlHttp.overrideMimeType('text/xml');
		} else if (window.ActiveXObject) {
			try {
				xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try {
					xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e) {
				}
			}
		}
		return xmlHttp;
	}

	function com$yonyou$dxyma$InfosController$evaljs(js) {
		eval(js)
	}

	function com$yonyou$dxyma$InfosController$button0_onclick(sender, args) {

	}

	function com$yonyou$dxyma$InfosController$onitemclick(sender, args) {
		//当前选中行
		var data = $id("listviewdefine0").get("row");
		var vs = data["material_code"]

	}

	function com$yonyou$dxyma$InfosController$viewPage0_onload(sender, args) {
		var wk_id = "wewew";
		var myparam = {
			"nchostip":"10.211.55.3",//nc服务器ip
			"nchostport":"8001",//nc服务器端口
			"actiontype" : "66602",
			"param" : {
				"wcid" : wk_id
			}
		};
		//MA服务器ip和端口
		$service.writeConfig({
            "host" : "10.211.55.3",//向configure中写入host键值
            "port" : "8002"//向configure中写入port键值
        })
		$service.callAction({
            "viewid" : "com.yonyou.mes.HttpCaller",//部署在MA上的Controller的包名
            "action" : "call",//后台Controller的方法名,
            "params" : {"myparam":myparam},//自定义参数，json格式
            "autoDataBinding" : false,//请求回来的数据会在Context中，是否进行数据绑定，默认不绑定
            "contextmapping" : "result",//将返回结果映射到指定的Context字段上，支持fieldName和xx.xxx.fieldName字段全路径，如未指定contextmapping则替换整个Context
            "callback" : "callsuccess()",//请求成功后回调js方法
            "error" : "callerror()"//请求失败回调的js方法
        })
	}
	function callsuccess(){
		var result = $ctx.get("result");
		$alert(result);
		var jsonobj = JSON.parse(result);
		var status = jsonobj["statuscode"];
		if (status == 0) {
			var datas = jsonobj["datas"];
			var newDatas = [];
			for (var i = 0; i <= datas.length - 1; i++) {
				var data = datas[i];
				newDatas[i].material_code = data.material.name;
				newDatas[i].material_num = data.nnum + data.material.dw;
			}
			var contextdaras = {
				"datas" : newDatas
			};
			$ctx.push(contextdaras);
		} else {
			alert("check datas");
		}
	}
	function callerror(){
		$alert("error");
	}
	 


	com.yonyou.dxyma.InfosController.prototype = {
		viewPage0_onload : com$yonyou$dxyma$InfosController$viewPage0_onload,
		onitemclick : com$yonyou$dxyma$InfosController$onitemclick,
		button0_onclick : com$yonyou$dxyma$InfosController$button0_onclick,
		initialize : com$yonyou$dxyma$InfosController$initialize,
		evaljs : com$yonyou$dxyma$InfosController$evaljs
	};
	com.yonyou.dxyma.InfosController.registerClass('com.yonyou.dxyma.InfosController', UMP.UI.Mvc.Controller);
} catch(e) {
	$e(e);
}
