//JavaScript Framework 2.0 Code
try {
	Type.registerNamespace('com.yonyou.dxyma.MachineController');
	com.yonyou.dxyma.MachineController = function() {
		com.yonyou.dxyma.MachineController.initializeBase(this);
		this.initialize();
	}
	var mac;
	function com$yonyou$dxyma$MachineController$initialize() {
		//you can programing by $ctx API
		//get the context data through $ctx.get()
		//set the context data through $ctx.push(json)
		//set the field of the context through $ctx.put(fieldName, fieldValue)
		//get the parameter of the context through $ctx.param(parameterName)
		//Demo Code:
		//    var str = $ctx.getString();      //获取当前Context对应的字符串
		//    alert($ctx.getString())          //alert当前Context对应的字符串
		//    var json = $ctx.getJSONObject(); //获取当前Context，返回值为json
		//    json["x"] = "a";        //为当前json增加字段
		//    json["y"] = [];           //为当前json增加数组
		//    $ctx.push(json);            //设置context，并自动调用数据绑定
		//
		//    put方法需手动调用databind()
		//    var x = $ctx.get("x");    //获取x字段值
		//    $ctx.put("x", "b");     //设置x字段值
		//    $ctx.put("x", "b");     //设置x字段值
		//    $ctx.databind();            //调用数据绑定才能将修改的字段绑定到控件上
		//    var p1 = $param.getString("p1");   //获取参数p2的值，返回一个字符串
		//    var p2 = $param.getJSONObject("p2");   //获取参数p3的值，返回一个JSON对象
		//    var p3 = $param.getJSONArray("p3");   //获取参数p1的值，返回一个数组

		//your initialize code below...

	}

	function com$yonyou$dxyma$MachineController$evaljs(js) {
		eval(js)
	}

	function com$yonyou$dxyma$MachineController$pageOnLoad(sender, args) {
		//$alert();
		mac = "粗拉机台#01";
		var context = {
			machine_codes : ["粗拉机台#01", "粗拉机台#02","粗拉机台#03","细拉机台#01", "细拉机台#02","细拉机台#03"]
		}
		$ctx.push(context)
	}

	function onChange() {
		//$alert("编号:" + $ctx.getString("machine_code"));
		var macs = $ctx.getString("machine_code");
		//$alert(macs);
		mac = macs;
	}

	function com$yonyou$dxyma$MachineController$button2_onclick(sender, args) {
		//$alert(mac);
		$view.open({
			"viewid" : "com.yonyou.dxyma.Infos", //目标页面（首字母大写）全名
			"isKeep" : "false",
			"data" : mac,
			"ip":$param.getString("ip"),
       		"port":$param.getString("port")
			//"callback" : "myCallBack()"//关闭viewid页面后，执行的回调方法
		})

	}



function com$yonyou$dxyma$MachineController$picker0_onload(sender, args){

}	com.yonyou.dxyma.MachineController.prototype = {
    picker0_onload : com$yonyou$dxyma$MachineController$picker0_onload,
		button2_onclick : com$yonyou$dxyma$MachineController$button2_onclick,
		pageOnLoad : com$yonyou$dxyma$MachineController$pageOnLoad,
		initialize : com$yonyou$dxyma$MachineController$initialize,
		evaljs : com$yonyou$dxyma$MachineController$evaljs
	};
	com.yonyou.dxyma.MachineController.registerClass('com.yonyou.dxyma.MachineController', UMP.UI.Mvc.Controller);
} catch(e) {
	$e(e);
}
