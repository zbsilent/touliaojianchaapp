//JavaScript Framework 2.0 Code
try {
	Type.registerNamespace('com.yonyou.dxyma.ConfirmController');
	com.yonyou.dxyma.ConfirmController = function() {
		com.yonyou.dxyma.ConfirmController.initializeBase(this);
		this.initialize();
	}
	function com$yonyou$dxyma$ConfirmController$initialize() {
		//you can programing by $ctx API
		//get the context data through $ctx.get()
		//set the context data through $ctx.push(json)
		//set the field of the context through $ctx.put(fieldName, fieldValue)
		//get the parameter of the context through $ctx.param(parameterName)
		//Demo Code:
		//    var str = $ctx.getString();      //获取当前Context对应的字符串
		//    alert($ctx.getString())          //alert当前Context对应的字符串
		//    var json = $ctx.getJSONObject(); //获取当前Context，返回值为json
		//    json["x"] = "a";        //为当前json增加字段
		//    json["y"] = [];           //为当前json增加数组
		//    $ctx.push(json);            //设置context，并自动调用数据绑定
		//
		//    put方法需手动调用databind()
		//    var x = $ctx.get("x");    //获取x字段值
		//    $ctx.put("x", "b");     //设置x字段值
		//    $ctx.put("x", "b");     //设置x字段值
		//    $ctx.databind();            //调用数据绑定才能将修改的字段绑定到控件上
		//    var p1 = $param.getString("p1");   //获取参数p2的值，返回一个字符串
		//    var p2 = $param.getJSONObject("p2");   //获取参数p3的值，返回一个JSON对象
		//    var p3 = $param.getJSONArray("p3");   //获取参数p1的值，返回一个数组

		//your initialize code below...

	}

	function com$yonyou$dxyma$ConfirmController$evaljs(js) {
		eval(js)
	}
var data;
	//界面打开后加载数据
	function com$yonyou$dxyma$ConfirmController$pageOnLoad(sender, args) {
		//$alert("====ZHIXING====");
		// 加载机台物料信息
		data = $param.getJSONObject("data");
		//$alert(str);
		var jt_code = data["jt_code"];
		var parmater = data["data"];
		var zl_code = parmater["material_order"];
		var cp_name = parmater["material_name"];
		$id("jtlabel").set("value",jt_code);
		$id("zllabel").set("value",zl_code);
		$id("cplabel").set("value",cp_name);
		var scanString = $param.getString("scannerData");
		try{
			var scanParmater = JSON.parse(scanString);
			//$alert(scanString);
			if(scanParmater["material_name"]==cp_name){
				$id("gyslabel").set("value",scanParmater["gys"]);
				$id("gzlabel").set("value",scanParmater["gz"]);
				$id("lphlabel").set("value",scanParmater["batchcode"]);
				$id("jhlabel").set("value",scanParmater["jh"]);
				$id("weightlabel").set("value",scanParmater["material_weight"]);
			}else{
				$id("gyslabel").set("value",scanParmater["gys"]);
				$id("gzlabel").set("value",scanParmater["gz"]);
				$id("lphlabel").set("value",scanParmater["batchcode"]);
				$id("jhlabel").set("value",scanParmater["jh"]);
				$id("weightlabel").set("value",scanParmater["material_weight"]);
				$id("label1").set("value","不匹配");
				$id("label1").set("background","#ff0000");
				$id("button2").set("value","重新扫码");
			}
		}catch(err){
			$alert("二维码信息错误，请检查");
			$id("label1").set("value","不匹配");
			$id("label1").set("background","#ff0000");
			$id("button2").set("value","重新扫码");
		}
	}
function com$yonyou$dxyma$ConfirmController$button2_onclick(sender, args){
	var parmater = data["data"];
	if($id("label1").get("value")=="匹配"){
		$alert("投料成功");
		var wk_id = data["jt_code"];
		$view.open({
        	"viewid" : "com.yonyou.dxyma.Infos",//目标页面（首字母大写）全名
        	"isKeep" : "false",
        	"data": wk_id,
        	"no": parmater["no"],
        	"num":$id("weightlabel").get("value")
        	//"callback" : "myCallBack()"//关闭viewid页面后，执行的回调方法
    	})
	}else{
		$view.open({
        	"viewid" : "com.yonyou.dxyma.Scan",//目标页面（首字母大写）全名
        	"isKeep" : "false",
        	"data": data,
        	"no": parmater["no"],
        	"num":0
    	})
	}
}
function com$yonyou$dxyma$ConfirmController$onkeydown(sender, args){
	var parmater = data["data"];
	$view.open({
        "viewid" : "com.yonyou.dxyma.Scan",//目标页面（首字母大写）全名
        "isKeep" : "false",
        "data": data,
        "no": parmater["no"],
        "num":0
    })
}	
com.yonyou.dxyma.ConfirmController.prototype = {
    onkeydown : com$yonyou$dxyma$ConfirmController$onkeydown,
    button2_onclick : com$yonyou$dxyma$ConfirmController$button2_onclick,
		pageOnLoad : com$yonyou$dxyma$ConfirmController$pageOnLoad,
		initialize : com$yonyou$dxyma$ConfirmController$initialize,
		evaljs : com$yonyou$dxyma$ConfirmController$evaljs
	};
	com.yonyou.dxyma.ConfirmController.registerClass('com.yonyou.dxyma.ConfirmController', UMP.UI.Mvc.Controller);
} catch(e) {
	$e(e);
}
function show() {
	alert("绑定字段的值：" + $id("wtogglebuttongroup0").getAttribute("selectedValue"));
}
