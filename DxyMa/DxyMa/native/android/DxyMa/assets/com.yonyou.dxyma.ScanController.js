//JavaScript Framework 2.0 Code
try{
Type.registerNamespace('com.yonyou.dxyma.ScanController');
com.yonyou.dxyma.ScanController = function() {
    com.yonyou.dxyma.ScanController.initializeBase(this);
    this.initialize();
}
function com$yonyou$dxyma$ScanController$initialize(){
    //you can programing by $ctx API
    //get the context data through $ctx.get()
    //set the context data through $ctx.push(json)
    //set the field of the context through $ctx.put(fieldName, fieldValue)
    //get the parameter of the context through $ctx.param(parameterName)
    //Demo Code:
    //    var str = $ctx.getString();      //获取当前Context对应的字符串
    //    alert($ctx.getString())          //alert当前Context对应的字符串
    //    var json = $ctx.getJSONObject(); //获取当前Context，返回值为json
    //    json["x"] = "a";        //为当前json增加字段
    //    json["y"] = [];           //为当前json增加数组
    //    $ctx.push(json);            //设置context，并自动调用数据绑定
    //    
    //    put方法需手动调用databind()
    //    var x = $ctx.get("x");    //获取x字段值
    //    $ctx.put("x", "b");     //设置x字段值
    //    $ctx.put("x", "b");     //设置x字段值
    //    $ctx.databind();            //调用数据绑定才能将修改的字段绑定到控件上
    //    var p1 = $param.getString("p1");   //获取参数p2的值，返回一个字符串
    //    var p2 = $param.getJSONObject("p2");   //获取参数p3的值，返回一个JSON对象
    //    var p3 = $param.getJSONArray("p3");   //获取参数p1的值，返回一个数组
    
    //your initialize code below...
    
}
    
function com$yonyou$dxyma$ScanController$evaljs(js){
    eval(js)
}
function com$yonyou$dxyma$ScanController$twodimensioncode0_onscansuccess(sender, args){
    var scanString = $ctx.getString("result");
    $view.open({
        "viewid" : "com.yonyou.dxyma.Confirm",//目标页面（首字母大写）全名
        "isKeep" : "false",
        "data":confirm_data,
        "scannerData":scanString
        //"callback" : "myCallBack()"//关闭viewid页面后，执行的回调方法
    })
}
var confirm_data;
function com$yonyou$dxyma$ScanController$viewPage0_onload(sender, args){
	var datas = $param.getJSONObject("data");
	//$alert("getdata success");
	confirm_data = datas;
	/*$scanner.open({
        "bindfield" : "result"
    });*/
}
function com$yonyou$dxyma$ScanController$onkeydown(sender, args){
	$view.open({
		"viewid" : "com.yonyou.dxyma.Infos", //目标页面（首字母大写）全名
		"isKeep" : "false",
		"data" : confirm_data["jt_code"],
		"no": $param.getString("no"),
        "num":$param.getString("num")
		//"callback" : "myCallBack()"//关闭viewid页面后，执行的回调方法
	})
}
com.yonyou.dxyma.ScanController.prototype = {
    onkeydown : com$yonyou$dxyma$ScanController$onkeydown,
    viewPage0_onload : com$yonyou$dxyma$ScanController$viewPage0_onload,
    twodimensioncode0_onscansuccess : com$yonyou$dxyma$ScanController$twodimensioncode0_onscansuccess,
    initialize : com$yonyou$dxyma$ScanController$initialize,
    evaljs : com$yonyou$dxyma$ScanController$evaljs
};
com.yonyou.dxyma.ScanController.registerClass('com.yonyou.dxyma.ScanController',UMP.UI.Mvc.Controller);
}catch(e){$e(e);}
