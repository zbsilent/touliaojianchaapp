package com.yonyou.dxyma;

import com.yonyou.uap.um.application.ApplicationContext;
import com.yonyou.uap.um.base.*;
import com.yonyou.uap.um.common.*;
import com.yonyou.uap.um.third.*;
import com.yonyou.uap.um.control.*;
import com.yonyou.uap.um.core.*;
import com.yonyou.uap.um.binder.*;
import com.yonyou.uap.um.runtime.*;
import com.yonyou.uap.um.lexer.*;
import com.yonyou.uap.um.widget.*;
import com.yonyou.uap.um.widget.UmpSlidingLayout.SlidingViewType;
import com.yonyou.uap.um.log.ULog;
import java.util.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import android.webkit.*;
import android.content.*;
import android.graphics.*;
import android.widget.ImageView.ScaleType;

public abstract class InfosActivity extends UMWindowActivity {

	protected UMWindow Infos = null;
protected XVerticalLayout viewPage0 = null;
protected XVerticalLayout panel3 = null;
protected XHorizontalLayout panel4 = null;
protected UMLabel label1 = null;
protected UMLabel label7 = null;
protected XHorizontalLayout panel0 = null;
protected UMLabel label2 = null;
protected UMLabel label3 = null;
protected UMLabel label8 = null;
protected UMLabel label6 = null;
protected UMListViewBase listviewdefine0 = null;
protected XHorizontalLayout panel1 = null;
protected UMLabel label4 = null;
protected UMLabel label5 = null;
protected UMLabel label9 = null;
protected UMLabel label0 = null;
protected XHorizontalLayout panel2 = null;
protected UMImageButton imagebutton0 = null;

	
	protected final static int ID_INFOS = 2122166060;
protected final static int ID_VIEWPAGE0 = 1577959634;
protected final static int ID_PANEL3 = 1532605399;
protected final static int ID_PANEL4 = 1067701074;
protected final static int ID_LABEL1 = 971337700;
protected final static int ID_LABEL7 = 256295205;
protected final static int ID_PANEL0 = 221196394;
protected final static int ID_LABEL2 = 646955688;
protected final static int ID_LABEL3 = 773220534;
protected final static int ID_LABEL8 = 642110729;
protected final static int ID_LABEL6 = 34390210;
protected final static int ID_LISTVIEWDEFINE0 = 1774747782;
protected final static int ID_PANEL1 = 350934797;
protected final static int ID_LABEL4 = 1333160172;
protected final static int ID_LABEL5 = 1094503096;
protected final static int ID_LABEL9 = 1703154127;
protected final static int ID_LABEL0 = 1173289345;
protected final static int ID_PANEL2 = 1367249805;
protected final static int ID_IMAGEBUTTON0 = 606694823;

	
	
	@Override
	public String getControllerName() {
		return "InfosController";
	}
	
	@Override
	public String getContextName() {
		return "";
	}

	@Override
	public String getNameSpace() {
		return "com.yonyou.dxyma";
	}

	protected void onCreate(Bundle savedInstanceState) {
		ULog.logLC("onCreate", this);
		super.onCreate(savedInstanceState);
        onInit(savedInstanceState);
        	super.setEvent("onkeydown", "action:infos_onkeydown");

	}
	
	@Override
	protected void onStart() {
		ULog.logLC("onStart", this);
		
		super.onStart();
	}

	@Override
	protected void onRestart() {
		ULog.logLC("onRestart", this);
		
		super.onRestart();
	}

	@Override
	protected void onResume() {
		ULog.logLC("onResume", this);
		
		super.onResume();
	}

	@Override
	protected void onPause() {
		ULog.logLC("onPause", this);
		
		super.onPause();
	}

	@Override
	protected void onStop() {
		ULog.logLC("onStop", this);
		
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		ULog.logLC("onDestroy", this);
		
		super.onDestroy();
	}
	
	public  void onInit(Bundle savedInstanceState) {
		ULog.logLC("onInit", this);
		UMActivity context = this;
		registerControl();
		this.getContainer();
		
		/*
		 new Thread() {
			 public void run() {
				 UMPDebugClient.startServer();
			 }
		 }.start();
		*/
		String sys_debug = ApplicationContext.getCurrent(this).getValue("sys_debug");
		if (sys_debug != null && sys_debug.equalsIgnoreCase("true")) {
			UMPDebugClient.waitForDebug();
		}

		IBinderGroup binderGroup = this;
		currentPage = getCurrentWindow(context, binderGroup);
super.setContentView(currentPage);

		
	}
	
	private void registerControl() {
		  idmap.put("Infos",ID_INFOS);
  idmap.put("viewPage0",ID_VIEWPAGE0);
  idmap.put("panel3",ID_PANEL3);
  idmap.put("panel4",ID_PANEL4);
  idmap.put("label1",ID_LABEL1);
  idmap.put("label7",ID_LABEL7);
  idmap.put("panel0",ID_PANEL0);
  idmap.put("label2",ID_LABEL2);
  idmap.put("label3",ID_LABEL3);
  idmap.put("label8",ID_LABEL8);
  idmap.put("label6",ID_LABEL6);
  idmap.put("listviewdefine0",ID_LISTVIEWDEFINE0);
  idmap.put("panel1",ID_PANEL1);
  idmap.put("label4",ID_LABEL4);
  idmap.put("label5",ID_LABEL5);
  idmap.put("label9",ID_LABEL9);
  idmap.put("label0",ID_LABEL0);
  idmap.put("panel2",ID_PANEL2);
  idmap.put("imagebutton0",ID_IMAGEBUTTON0);

	}
	
	public void onLoad() {
		ULog.logLC("onLoad", this);
		if(currentPage!=null) {
			currentPage.onLoad();
		}
	
		{ //viewPage0 - action:viewpage0_onload
    UMEventArgs args = new UMEventArgs(InfosActivity.this);
    actionViewpage0_onload(viewPage0,args);

}

	}
	
	public void onDatabinding() {
		ULog.logLC("onDatabinding", this);
		super.onDatabinding();
		
	}
	
	@Override
	public void onReturn(String methodName, Object returnValue) {
		
	}

	@Override
	public void onAfterInit() {
		ULog.logLC("onAfterInit", this);
		
		onLoad();
	}
	
		@Override
	public Map<String,String> getPlugout(String id) {
		XJSON from = this.getUMContext();
		Map<String,String> r = super.getPlugout(id);
		
		return r;	
	}
	
	
	
	public View getPanel4View(final UMActivity context,IBinderGroup binderGroup) {
panel4 = (XHorizontalLayout)ThirdControl.createControl(new XHorizontalLayout(context),ID_PANEL4
,"margin-left","0"
,"width","fill"
,"valign","center"
,"layout-type","vbox"
,"halign","LEFT"
,"margin-right","0"
,"height","50"
);
label1 = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_LABEL1
,"color","#000000"
,"heightwrap","20.0"
,"padding-left","5"
,"width","80"
,"font-size","14"
,"valign","center"
,"font-family","default"
,"layout-type","hbox"
,"halign","left"
,"content","机台指令"
,"height","wrap"
);
panel4.addView(label1);
label7 = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_LABEL7
,"margin-left","180"
,"color","#000000"
,"width","90.0"
,"font-size","14"
,"valign","center"
,"font-family","default"
,"layout-type","hbox"
,"halign","left"
,"margin-right","15"
,"height","20"
);
panel4.addView(label7);

return panel4;
}
public View getPanel0View(final UMActivity context,IBinderGroup binderGroup) {
panel0 = (XHorizontalLayout)ThirdControl.createControl(new XHorizontalLayout(context),ID_PANEL0
,"margin-left","0"
,"width","fill"
,"valign","center"
,"layout-type","vbox"
,"halign","LEFT"
,"margin-right","0"
,"height","30"
);
label2 = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_LABEL2
,"color","#000000"
,"width","130.0"
,"font-size","14"
,"valign","center"
,"font-family","default"
,"layout-type","hbox"
,"halign","center"
,"content","物料"
,"height","30"
);
panel0.addView(label2);
label3 = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_LABEL3
,"color","#000000"
,"width","90.0"
,"font-size","14"
,"valign","center"
,"font-family","default"
,"layout-type","hbox"
,"halign","center"
,"content","待投料量"
,"height","30"
);
panel0.addView(label3);
label8 = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_LABEL8
,"color","#000000"
,"width","80.0"
,"font-size","14"
,"valign","center"
,"font-family","default"
,"layout-type","hbox"
,"halign","center"
,"content","指令状态"
,"height","30"
);
panel0.addView(label8);
label6 = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_LABEL6
,"color","#000000"
,"width","50.0"
,"font-size","14"
,"valign","center"
,"font-family","default"
,"layout-type","hbox"
,"halign","center"
,"content","选中"
,"height","30"
);
panel0.addView(label6);

return panel0;
}
public View getPanel1View(final UMActivity context,IBinderGroup binderGroup) {
panel1 = (XHorizontalLayout)ThirdControl.createControl(new XHorizontalLayout(context),ID_PANEL1
,"border-bottom-color","#C7C7C7"
,"background","#FFFFFF"
,"border-bottom-width","1"
,"width","fill"
,"valign","center"
,"layout-type","vbox"
,"halign","LEFT"
,"height","30"
);
label4 = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_LABEL4
,"bindfield","material_name"
,"color","#000000"
,"width","130.0"
,"font-size","14"
,"valign","center"
,"font-family","default"
,"layout-type","hbox"
,"halign","center"
,"content","label"
,"height","30"
);
panel1.addView(label4);
label5 = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_LABEL5
,"bindfield","material_weight"
,"color","#000000"
,"width","90.0"
,"font-size","14"
,"valign","center"
,"font-family","default"
,"layout-type","hbox"
,"halign","center"
,"content","label"
,"height","30"
);
panel1.addView(label5);
label9 = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_LABEL9
,"bindfield","order_state"
,"color","#000000"
,"width","80.0"
,"font-size","14"
,"valign","center"
,"font-family","default"
,"layout-type","hbox"
,"halign","center"
,"content","label"
,"height","30"
);
panel1.addView(label9);
label0 = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_LABEL0
,"bindfield","symbol"
,"color","#400040"
,"width","50.0"
,"font-size","14"
,"valign","center"
,"font-family","default"
,"layout-type","hbox"
,"halign","center"
,"content","label"
,"margin-right","10"
,"height","30"
);
panel1.addView(label0);

return panel1;
}
public View getListviewdefine0View(final UMActivity context,IBinderGroup binderGroup) {
listviewdefine0 = (UMListViewBase)ThirdControl.createControl(new UMListViewBase(context),ID_LISTVIEWDEFINE0
,"layout","vbox"
,"margin-left","0"
,"bindfield","datas"
,"collapsed","true"
,"width","fill"
,"valign","TOP"
,"onitemclick","action:listviewdefine0_onitemclick"
,"layout-type","vbox"
,"halign","center"
,"height","fill"
);
listviewdefine0.addListItemView("getPanel1View");

return listviewdefine0;
}
public View getPanel3View(final UMActivity context,IBinderGroup binderGroup) {
panel3 = (XVerticalLayout)ThirdControl.createControl(new XVerticalLayout(context),ID_PANEL3
,"margin-left","10"
,"width","fill"
,"weight","1"
,"valign","TOP"
,"layout-type","vbox"
,"halign","center"
,"margin-right","10"
,"height","0"
);
View panel4 = (View) getPanel4View((UMActivity)context,binderGroup);
panel3.addView(panel4);
View panel0 = (View) getPanel0View((UMActivity)context,binderGroup);
panel3.addView(panel0);
View listviewdefine0 = (View) getListviewdefine0View((UMActivity)context,binderGroup);
panel3.addView(listviewdefine0);

return panel3;
}
public View getPanel2View(final UMActivity context,IBinderGroup binderGroup) {
panel2 = (XHorizontalLayout)ThirdControl.createControl(new XHorizontalLayout(context),ID_PANEL2
,"margin-left","10"
,"width","fill"
,"valign","center"
,"layout-type","vbox"
,"halign","RIGHT"
,"margin-bottom","20"
,"margin-right","10"
,"height","70"
);
imagebutton0 = (UMImageButton)ThirdControl.createControl(new UMImageButton(context),ID_IMAGEBUTTON0
,"font-pressed-color","#e50011"
,"color","#919191"
,"icon-pressed-image","tab_search.png"
,"onclick","action:imagebutton0_onclick"
,"icon-height","30"
,"font-size","10"
,"valign","center"
,"icon-text-spacing","3"
,"widthwrap","64.0"
,"imagebuttontype","icon"
,"heightwrap","49.0"
,"icon-background-image","tab_search_touch.png"
,"istogglebutton","false"
,"width","wrap"
,"checked","false"
,"font-family","default"
,"layout-type","hbox"
,"halign","center"
,"icon-width","30"
,"value","检查"
,"height","wrap"
);
panel2.addView(imagebutton0);

return panel2;
}
public View getViewPage0View(final UMActivity context,IBinderGroup binderGroup) {
viewPage0 = (XVerticalLayout)ThirdControl.createControl(new XVerticalLayout(context),ID_VIEWPAGE0
,"background","#F7F8F8"
,"width","fill"
,"valign","TOP"
,"layout-type","vbox"
,"halign","center"
,"onload","action:viewpage0_onload"
,"height","fill"
);
View panel3 = (View) getPanel3View((UMActivity)context,binderGroup);
viewPage0.addView(panel3);
View panel2 = (View) getPanel2View((UMActivity)context,binderGroup);
viewPage0.addView(panel2);

return viewPage0;
}
public UMWindow getCurrentWindow(final UMActivity context,IBinderGroup binderGroup) {
Infos = (UMWindow)ThirdControl.createControl(new UMWindow(context),ID_INFOS
,"layout","vbox"
,"controller","InfosController"
,"namespace","com.yonyou.dxyma"
,"width","fill"
,"valign","TOP"
,"onkeydown","action:infos_onkeydown"
,"layout-type","linear"
,"halign","center"
,"height","fill"
);
View viewPage0 = (View) getViewPage0View((UMActivity)context,binderGroup);
Infos.addView(viewPage0);

return (UMWindow)Infos;
}

	
	public void actionListviewdefine0_onitemclick(View control, UMEventArgs args) {
    args.put("containerName","");
    args.put("language","javascript");
    String actionid = "listviewdefine0_onitemclick";
  ActionProcessor.exec(this, actionid, args);
  this.getContainer().exec(actionid, "this.onItemClick()",UMActivity.getViewId(control),args);
}
public void actionImagebutton0_onclick(View control, UMEventArgs args) {
    args.put("containerName","");
    args.put("language","javascript");
    String actionid = "imagebutton0_onclick";
  ActionProcessor.exec(this, actionid, args);
  this.getContainer().exec(actionid, "this.onQueryButClick()",UMActivity.getViewId(control),args);
}
public void actionInfos_onkeydown(View control, UMEventArgs args) {
    args.put("containerName","");
    args.put("language","javascript");
    String actionid = "infos_onkeydown";
  ActionProcessor.exec(this, actionid, args);
  this.getContainer().exec(actionid, "this.onkeydown()",UMActivity.getViewId(control),args);
}
public void actionViewpage0_onload(View control, UMEventArgs args) {
    args.put("containerName","");
    args.put("language","javascript");
    String actionid = "viewpage0_onload";
  ActionProcessor.exec(this, actionid, args);
  this.getContainer().exec(actionid, "this.viewPage0_onload()",UMActivity.getViewId(control),args);
}


}
