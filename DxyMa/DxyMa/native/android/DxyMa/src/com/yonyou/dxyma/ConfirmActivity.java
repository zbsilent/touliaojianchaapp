package com.yonyou.dxyma;

import com.yonyou.uap.um.application.ApplicationContext;
import com.yonyou.uap.um.base.*;
import com.yonyou.uap.um.common.*;
import com.yonyou.uap.um.third.*;
import com.yonyou.uap.um.control.*;
import com.yonyou.uap.um.core.*;
import com.yonyou.uap.um.binder.*;
import com.yonyou.uap.um.runtime.*;
import com.yonyou.uap.um.lexer.*;
import com.yonyou.uap.um.widget.*;
import com.yonyou.uap.um.widget.UmpSlidingLayout.SlidingViewType;
import com.yonyou.uap.um.log.ULog;
import java.util.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import android.webkit.*;
import android.content.*;
import android.graphics.*;
import android.widget.ImageView.ScaleType;

public abstract class ConfirmActivity extends UMWindowActivity {

	protected UMWindow Confirm = null;
protected XVerticalLayout viewPage0 = null;
protected XHorizontalLayout navigatorbar0 = null;
protected UMButton button0 = null;
protected UMLabel label0 = null;
protected UMScrollView Scrollview_wpanel6 = null;
protected XVerticalLayout wpanel6 = null;
protected XHorizontalLayout wpanel0 = null;
protected UMLabel wlabel0 = null;
protected UMLabel jtlabel = null;
protected XHorizontalLayout wpanel1 = null;
protected UMLabel wlabel1 = null;
protected UMLabel zllabel = null;
protected XHorizontalLayout wpanel3 = null;
protected UMLabel wlabel3 = null;
protected UMLabel cplabel = null;
protected XVerticalLayout panel1 = null;
protected XHorizontalLayout wpanel5 = null;
protected UMLabel wlabel5 = null;
protected UMLabel label5 = null;
protected UMLabel label6 = null;
protected XHorizontalLayout wpane21 = null;
protected UMLabel wlabe21 = null;
protected UMLabel gyslabel = null;
protected XHorizontalLayout wpane22 = null;
protected UMLabel wlabe22 = null;
protected UMLabel gzlabel = null;
protected XHorizontalLayout wpane23 = null;
protected UMLabel wlabe23 = null;
protected UMLabel lphlabel = null;
protected XHorizontalLayout wpane24 = null;
protected UMLabel wlabe24 = null;
protected UMLabel jhlabel = null;
protected XHorizontalLayout wpane25 = null;
protected UMLabel wlabe25 = null;
protected UMLabel weightlabel = null;
protected XHorizontalLayout wpanel9 = null;
protected UMLabel wlabel9 = null;
protected UMLabel label1 = null;
protected XHorizontalLayout panel0 = null;
protected UMButton button2 = null;

	
	protected final static int ID_CONFIRM = 596208333;
protected final static int ID_VIEWPAGE0 = 1890406522;
protected final static int ID_NAVIGATORBAR0 = 724864608;
protected final static int ID_BUTTON0 = 1775780728;
protected final static int ID_LABEL0 = 1021134723;
protected final static int ID_SCROLLVIEW_WPANEL6 = 973273975;
protected final static int ID_WPANEL6 = 123029772;
protected final static int ID_WPANEL0 = 977140020;
protected final static int ID_WLABEL0 = 831917400;
protected final static int ID_JTLABEL = 359093521;
protected final static int ID_WPANEL1 = 1549832990;
protected final static int ID_WLABEL1 = 1763434921;
protected final static int ID_ZLLABEL = 1980377278;
protected final static int ID_WPANEL3 = 627559758;
protected final static int ID_WLABEL3 = 1465778551;
protected final static int ID_CPLABEL = 1701592077;
protected final static int ID_PANEL1 = 532463333;
protected final static int ID_WPANEL5 = 425442824;
protected final static int ID_WLABEL5 = 2080916470;
protected final static int ID_LABEL5 = 661476028;
protected final static int ID_LABEL6 = 448387372;
protected final static int ID_WPANE21 = 1135667853;
protected final static int ID_WLABE21 = 1765156264;
protected final static int ID_GYSLABEL = 949882561;
protected final static int ID_WPANE22 = 1238825645;
protected final static int ID_WLABE22 = 1602651216;
protected final static int ID_GZLABEL = 1841419706;
protected final static int ID_WPANE23 = 630285266;
protected final static int ID_WLABE23 = 795358530;
protected final static int ID_LPHLABEL = 92198465;
protected final static int ID_WPANE24 = 702849430;
protected final static int ID_WLABE24 = 1065199469;
protected final static int ID_JHLABEL = 1504956021;
protected final static int ID_WPANE25 = 351702027;
protected final static int ID_WLABE25 = 1751980563;
protected final static int ID_WEIGHTLABEL = 256840619;
protected final static int ID_WPANEL9 = 397897897;
protected final static int ID_WLABEL9 = 283490186;
protected final static int ID_LABEL1 = 1219693646;
protected final static int ID_PANEL0 = 738047838;
protected final static int ID_BUTTON2 = 79284484;

	
	
	@Override
	public String getControllerName() {
		return "ConfirmController";
	}
	
	@Override
	public String getContextName() {
		return "";
	}

	@Override
	public String getNameSpace() {
		return "com.yonyou.dxyma";
	}

	protected void onCreate(Bundle savedInstanceState) {
		ULog.logLC("onCreate", this);
		super.onCreate(savedInstanceState);
        onInit(savedInstanceState);
        	super.setEvent("onkeydown", "action:confirm_onkeydown");

	}
	
	@Override
	protected void onStart() {
		ULog.logLC("onStart", this);
		
		super.onStart();
	}

	@Override
	protected void onRestart() {
		ULog.logLC("onRestart", this);
		
		super.onRestart();
	}

	@Override
	protected void onResume() {
		ULog.logLC("onResume", this);
		
		super.onResume();
	}

	@Override
	protected void onPause() {
		ULog.logLC("onPause", this);
		
		super.onPause();
	}

	@Override
	protected void onStop() {
		ULog.logLC("onStop", this);
		
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		ULog.logLC("onDestroy", this);
		
		super.onDestroy();
	}
	
	public  void onInit(Bundle savedInstanceState) {
		ULog.logLC("onInit", this);
		UMActivity context = this;
		registerControl();
		this.getContainer();
		
		/*
		 new Thread() {
			 public void run() {
				 UMPDebugClient.startServer();
			 }
		 }.start();
		*/
		String sys_debug = ApplicationContext.getCurrent(this).getValue("sys_debug");
		if (sys_debug != null && sys_debug.equalsIgnoreCase("true")) {
			UMPDebugClient.waitForDebug();
		}

		IBinderGroup binderGroup = this;
		currentPage = getCurrentWindow(context, binderGroup);
super.setContentView(currentPage);

		
	}
	
	private void registerControl() {
		  idmap.put("Confirm",ID_CONFIRM);
  idmap.put("viewPage0",ID_VIEWPAGE0);
  idmap.put("navigatorbar0",ID_NAVIGATORBAR0);
  idmap.put("button0",ID_BUTTON0);
  idmap.put("label0",ID_LABEL0);
  idmap.put("Scrollview_wpanel6",ID_SCROLLVIEW_WPANEL6);
  idmap.put("wpanel6",ID_WPANEL6);
  idmap.put("wpanel0",ID_WPANEL0);
  idmap.put("wlabel0",ID_WLABEL0);
  idmap.put("jtlabel",ID_JTLABEL);
  idmap.put("wpanel1",ID_WPANEL1);
  idmap.put("wlabel1",ID_WLABEL1);
  idmap.put("zllabel",ID_ZLLABEL);
  idmap.put("wpanel3",ID_WPANEL3);
  idmap.put("wlabel3",ID_WLABEL3);
  idmap.put("cplabel",ID_CPLABEL);
  idmap.put("panel1",ID_PANEL1);
  idmap.put("wpanel5",ID_WPANEL5);
  idmap.put("wlabel5",ID_WLABEL5);
  idmap.put("label5",ID_LABEL5);
  idmap.put("label6",ID_LABEL6);
  idmap.put("wpane21",ID_WPANE21);
  idmap.put("wlabe21",ID_WLABE21);
  idmap.put("gyslabel",ID_GYSLABEL);
  idmap.put("wpane22",ID_WPANE22);
  idmap.put("wlabe22",ID_WLABE22);
  idmap.put("gzlabel",ID_GZLABEL);
  idmap.put("wpane23",ID_WPANE23);
  idmap.put("wlabe23",ID_WLABE23);
  idmap.put("lphlabel",ID_LPHLABEL);
  idmap.put("wpane24",ID_WPANE24);
  idmap.put("wlabe24",ID_WLABE24);
  idmap.put("jhlabel",ID_JHLABEL);
  idmap.put("wpane25",ID_WPANE25);
  idmap.put("wlabe25",ID_WLABE25);
  idmap.put("weightlabel",ID_WEIGHTLABEL);
  idmap.put("wpanel9",ID_WPANEL9);
  idmap.put("wlabel9",ID_WLABEL9);
  idmap.put("label1",ID_LABEL1);
  idmap.put("panel0",ID_PANEL0);
  idmap.put("button2",ID_BUTTON2);

	}
	
	public void onLoad() {
		ULog.logLC("onLoad", this);
		if(currentPage!=null) {
			currentPage.onLoad();
		}
	
		{ //viewPage0 - action:viewpage0_onload
    UMEventArgs args = new UMEventArgs(ConfirmActivity.this);
    actionViewpage0_onload(viewPage0,args);

}

	}
	
	public void onDatabinding() {
		ULog.logLC("onDatabinding", this);
		super.onDatabinding();
		
	}
	
	@Override
	public void onReturn(String methodName, Object returnValue) {
		
	}

	@Override
	public void onAfterInit() {
		ULog.logLC("onAfterInit", this);
		
		onLoad();
	}
	
		@Override
	public Map<String,String> getPlugout(String id) {
		XJSON from = this.getUMContext();
		Map<String,String> r = super.getPlugout(id);
		
		return r;	
	}
	
	
	
	public View getNavigatorbar0View(final UMActivity context,IBinderGroup binderGroup) {
navigatorbar0 = (XHorizontalLayout)ThirdControl.createControl(new XHorizontalLayout(context),ID_NAVIGATORBAR0
,"color","#323232"
,"padding-left","8"
,"font-size","17"
,"valign","center"
,"title","投料信息"
,"width","fill"
,"font-family","default"
,"background-image","navbar_login.png"
,"layout-type","vbox"
,"halign","LEFT"
,"padding-right","8"
,"height","44.0"
);
button0 = (UMButton)ThirdControl.createControl(new UMButton(context),ID_BUTTON0
,"font-pressed-color","#e50011"
,"color","#ffffff"
,"width","44"
,"font-size","20"
,"font-family","default"
,"valign","center"
,"background-image","icon_back.png"
,"layout-type","hbox"
,"halign","center"
,"height","44"
);
navigatorbar0.addView(button0);
label0 = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_LABEL0
,"width","0"
,"weight","1"
,"valign","center"
,"font-family","default"
,"layout-type","hbox"
,"halign","center"
,"height","fill"
);
navigatorbar0.addView(label0);

return navigatorbar0;
}
public View getWpanel0View(final UMActivity context,IBinderGroup binderGroup) {
wpanel0 = (XHorizontalLayout)ThirdControl.createControl(new XHorizontalLayout(context),ID_WPANEL0
,"width","fill"
,"valign","center"
,"background-image","list_row_down1.png"
,"layout-type","vbox"
,"halign","LEFT"
,"height","44"
);
wlabel0 = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_WLABEL0
,"margin-left","20"
,"color","#000000"
,"gradient-orientation","TOP_BOTTOM"
,"width","60.0"
,"font-size","14"
,"valign","center"
,"font-family","default"
,"layout-type","hbox"
,"halign","left"
,"content","机台号:"
,"height","43"
);
wpanel0.addView(wlabel0);
jtlabel = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_JTLABEL
,"color","#000000"
,"padding-left","4"
,"width","fill"
,"font-size","14"
,"valign","center"
,"font-family","default"
,"layout-type","hbox"
,"halign","center"
,"margin-right","15"
,"height","43"
);
wpanel0.addView(jtlabel);

return wpanel0;
}
public View getWpanel1View(final UMActivity context,IBinderGroup binderGroup) {
wpanel1 = (XHorizontalLayout)ThirdControl.createControl(new XHorizontalLayout(context),ID_WPANEL1
,"width","fill"
,"valign","center"
,"background-image","list_row_down1.png"
,"layout-type","vbox"
,"halign","LEFT"
,"height","44"
);
wlabel1 = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_WLABEL1
,"margin-left","20"
,"color","#000000"
,"width","60"
,"font-size","14"
,"valign","center"
,"font-family","default"
,"layout-type","hbox"
,"halign","left"
,"content","指令号:"
,"height","43"
);
wpanel1.addView(wlabel1);
zllabel = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_ZLLABEL
,"color","#000000"
,"padding-left","4"
,"width","fill"
,"font-size","14"
,"valign","center"
,"font-family","default"
,"layout-type","hbox"
,"halign","center"
,"margin-right","15"
,"height","43"
);
wpanel1.addView(zllabel);

return wpanel1;
}
public View getWpanel3View(final UMActivity context,IBinderGroup binderGroup) {
wpanel3 = (XHorizontalLayout)ThirdControl.createControl(new XHorizontalLayout(context),ID_WPANEL3
,"width","fill"
,"valign","center"
,"background-image","list_row_down1.png"
,"layout-type","vbox"
,"halign","LEFT"
,"height","44"
);
wlabel3 = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_WLABEL3
,"margin-left","20"
,"color","#000000"
,"width","60"
,"font-size","14"
,"valign","center"
,"font-family","default"
,"layout-type","hbox"
,"halign","left"
,"content","产品号:"
,"height","43"
);
wpanel3.addView(wlabel3);
cplabel = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_CPLABEL
,"color","#000000"
,"padding-left","4"
,"width","fill"
,"font-size","14"
,"valign","center"
,"font-family","default"
,"layout-type","hbox"
,"halign","center"
,"margin-right","15"
,"height","43"
);
wpanel3.addView(cplabel);

return wpanel3;
}
public View getWpanel5View(final UMActivity context,IBinderGroup binderGroup) {
wpanel5 = (XHorizontalLayout)ThirdControl.createControl(new XHorizontalLayout(context),ID_WPANEL5
,"width","fill"
,"valign","center"
,"background-image","list_row_down1.png"
,"layout-type","vbox"
,"halign","LEFT"
,"height","44"
);
wlabel5 = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_WLABEL5
,"margin-left","20"
,"color","#000000"
,"width","fill"
,"font-size","14"
,"valign","center"
,"font-family","default"
,"layout-type","hbox"
,"halign","left"
,"content","物料信息:"
,"height","43"
);
wpanel5.addView(wlabel5);
label5 = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_LABEL5
,"color","#000000"
,"padding-left","4"
,"width","fill"
,"font-size","14"
,"valign","center"
,"font-family","default"
,"layout-type","hbox"
,"halign","center"
,"content","label"
,"margin-right","15"
,"height","43"
);
wpanel5.addView(label5);
label6 = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_LABEL6
,"color","#000000"
,"padding-left","4"
,"width","fill"
,"font-size","14"
,"valign","center"
,"font-family","default"
,"layout-type","hbox"
,"halign","center"
,"content","label"
,"margin-right","15"
,"height","43"
);
wpanel5.addView(label6);

return wpanel5;
}
public View getWpane21View(final UMActivity context,IBinderGroup binderGroup) {
wpane21 = (XHorizontalLayout)ThirdControl.createControl(new XHorizontalLayout(context),ID_WPANE21
,"color","#000000"
,"width","fill"
,"valign","center"
,"background-image","list_row_down1.png"
,"layout-type","vbox"
,"halign","LEFT"
,"height","44"
);
wlabe21 = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_WLABE21
,"margin-left","40"
,"color","#000000"
,"width","60"
,"font-size","14"
,"valign","center"
,"font-family","default"
,"layout-type","hbox"
,"halign","left"
,"content","供应商:"
,"height","43"
);
wpane21.addView(wlabe21);
gyslabel = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_GYSLABEL
,"color","#000000"
,"padding-left","4"
,"width","fill"
,"font-size","14"
,"valign","center"
,"font-family","default"
,"layout-type","hbox"
,"halign","center"
,"margin-right","15"
,"height","43"
);
wpane21.addView(gyslabel);

return wpane21;
}
public View getWpane22View(final UMActivity context,IBinderGroup binderGroup) {
wpane22 = (XHorizontalLayout)ThirdControl.createControl(new XHorizontalLayout(context),ID_WPANE22
,"width","fill"
,"valign","center"
,"background-image","list_row_down1.png"
,"layout-type","vbox"
,"halign","LEFT"
,"height","44"
);
wlabe22 = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_WLABE22
,"margin-left","40"
,"color","#000000"
,"width","60"
,"font-size","14"
,"valign","center"
,"font-family","default"
,"layout-type","hbox"
,"halign","left"
,"content","钢种:"
,"height","43"
);
wpane22.addView(wlabe22);
gzlabel = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_GZLABEL
,"color","#000000"
,"padding-left","4"
,"width","fill"
,"font-size","14"
,"valign","center"
,"font-family","default"
,"layout-type","hbox"
,"halign","center"
,"margin-right","15"
,"height","43"
);
wpane22.addView(gzlabel);

return wpane22;
}
public View getWpane23View(final UMActivity context,IBinderGroup binderGroup) {
wpane23 = (XHorizontalLayout)ThirdControl.createControl(new XHorizontalLayout(context),ID_WPANE23
,"width","fill"
,"valign","center"
,"background-image","list_row_down1.png"
,"layout-type","vbox"
,"halign","LEFT"
,"height","44"
);
wlabe23 = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_WLABE23
,"margin-left","40"
,"color","#000000"
,"width","60"
,"font-size","14"
,"valign","center"
,"font-family","default"
,"layout-type","hbox"
,"halign","left"
,"content","炉批号:"
,"height","43"
);
wpane23.addView(wlabe23);
lphlabel = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_LPHLABEL
,"color","#000000"
,"padding-left","4"
,"width","fill"
,"font-size","14"
,"valign","center"
,"font-family","default"
,"layout-type","hbox"
,"halign","center"
,"margin-right","15"
,"height","43"
);
wpane23.addView(lphlabel);

return wpane23;
}
public View getWpane24View(final UMActivity context,IBinderGroup binderGroup) {
wpane24 = (XHorizontalLayout)ThirdControl.createControl(new XHorizontalLayout(context),ID_WPANE24
,"width","fill"
,"valign","center"
,"background-image","list_row_down1.png"
,"layout-type","vbox"
,"halign","LEFT"
,"height","44"
);
wlabe24 = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_WLABE24
,"margin-left","40"
,"color","#000000"
,"width","60"
,"font-size","14"
,"valign","center"
,"font-family","default"
,"layout-type","hbox"
,"halign","left"
,"content","卷号:"
,"height","44"
);
wpane24.addView(wlabe24);
jhlabel = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_JHLABEL
,"color","#000000"
,"padding-left","4"
,"width","fill"
,"font-size","14"
,"valign","center"
,"font-family","default"
,"layout-type","hbox"
,"halign","center"
,"margin-right","15"
,"height","43"
);
wpane24.addView(jhlabel);

return wpane24;
}
public View getWpane25View(final UMActivity context,IBinderGroup binderGroup) {
wpane25 = (XHorizontalLayout)ThirdControl.createControl(new XHorizontalLayout(context),ID_WPANE25
,"width","fill"
,"valign","center"
,"background-image","list_row_down1.png"
,"layout-type","vbox"
,"halign","LEFT"
,"height","44"
);
wlabe25 = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_WLABE25
,"margin-left","40"
,"color","#000000"
,"width","60"
,"font-size","14"
,"valign","center"
,"font-family","default"
,"layout-type","hbox"
,"halign","left"
,"content","重量:"
,"height","43"
);
wpane25.addView(wlabe25);
weightlabel = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_WEIGHTLABEL
,"color","#000000"
,"padding-left","4"
,"width","fill"
,"font-size","14"
,"valign","center"
,"font-family","default"
,"layout-type","hbox"
,"halign","center"
,"margin-right","15"
,"height","43"
);
wpane25.addView(weightlabel);

return wpane25;
}
public View getWpanel9View(final UMActivity context,IBinderGroup binderGroup) {
wpanel9 = (XHorizontalLayout)ThirdControl.createControl(new XHorizontalLayout(context),ID_WPANEL9
,"width","fill"
,"valign","center"
,"background-image","list_row_down1.png"
,"layout-type","vbox"
,"halign","LEFT"
,"height","43"
);
wlabel9 = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_WLABEL9
,"color","#000000"
,"font-size","14"
,"valign","center"
,"content","匹配结果:"
,"margin-left","20"
,"width","80"
,"font-family","default"
,"margin-top","2"
,"layout-type","hbox"
,"halign","left"
,"margin-bottom","2"
,"height","45"
);
wpanel9.addView(wlabel9);
label1 = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_LABEL1
,"color","#000000"
,"gradient-orientation","LEFT_RIGHT"
,"font-size","14"
,"valign","center"
,"content","匹配"
,"margin-right","30"
,"margin-left","30"
,"background","#00ff40"
,"width","150"
,"font-family","default"
,"layout-type","hbox"
,"halign","center"
,"height","30"
);
wpanel9.addView(label1);

return wpanel9;
}
public View getPanel0View(final UMActivity context,IBinderGroup binderGroup) {
panel0 = (XHorizontalLayout)ThirdControl.createControl(new XHorizontalLayout(context),ID_PANEL0
,"width","fill"
,"valign","center"
,"layout-type","vbox"
,"halign","LEFT"
,"height","60.0"
);
button2 = (UMButton)ThirdControl.createControl(new UMButton(context),ID_BUTTON2
,"font-pressed-color","#f2adb2"
,"color","#ffffff"
,"onclick","action:button2_onclick"
,"font-size","17"
,"weight","1"
,"valign","center"
,"margin-right","10"
,"margin-left","10"
,"background","#ff0000"
,"width","0"
,"font-family","default"
,"layout-type","hbox"
,"halign","center"
,"value","确认投料"
,"height","44"
);
panel0.addView(button2);

return panel0;
}
public View getWpanel6View(final UMActivity context,IBinderGroup binderGroup) {
wpanel6 = (XVerticalLayout)ThirdControl.createControl(new XVerticalLayout(context),ID_WPANEL6
,"background","#e5e5e5"
,"width","fill"
,"valign","TOP"
,"layout-type","vbox"
,"halign","center"
,"height","wrap"
);
View wpanel0 = (View) getWpanel0View((UMActivity)context,binderGroup);
wpanel6.addView(wpanel0);
View wpanel1 = (View) getWpanel1View((UMActivity)context,binderGroup);
wpanel6.addView(wpanel1);
View wpanel3 = (View) getWpanel3View((UMActivity)context,binderGroup);
wpanel6.addView(wpanel3);
panel1 = (XVerticalLayout)ThirdControl.createControl(new XVerticalLayout(context),ID_PANEL1
,"background","#808080"
,"width","fill"
,"valign","TOP"
,"layout-type","vbox"
,"halign","center"
,"height","5"
);
wpanel6.addView(panel1);
View wpanel5 = (View) getWpanel5View((UMActivity)context,binderGroup);
wpanel6.addView(wpanel5);
View wpane21 = (View) getWpane21View((UMActivity)context,binderGroup);
wpanel6.addView(wpane21);
View wpane22 = (View) getWpane22View((UMActivity)context,binderGroup);
wpanel6.addView(wpane22);
View wpane23 = (View) getWpane23View((UMActivity)context,binderGroup);
wpanel6.addView(wpane23);
View wpane24 = (View) getWpane24View((UMActivity)context,binderGroup);
wpanel6.addView(wpane24);
View wpane25 = (View) getWpane25View((UMActivity)context,binderGroup);
wpanel6.addView(wpane25);
View wpanel9 = (View) getWpanel9View((UMActivity)context,binderGroup);
wpanel6.addView(wpanel9);
View panel0 = (View) getPanel0View((UMActivity)context,binderGroup);
wpanel6.addView(panel0);

return wpanel6;
}
public View getScrollview_wpanel6View(final UMActivity context,IBinderGroup binderGroup) {
Scrollview_wpanel6 = (UMScrollView)ThirdControl.createControl(new UMScrollView(context),ID_SCROLLVIEW_WPANEL6
,"margin-left","1"
,"layout","vbox"
,"width","fill"
,"hscrollenabled","disabled"
,"layout-type","vbox"
,"height","fill"
,"margin-right","1"
);
View wpanel6 = (View) getWpanel6View((UMActivity)context,binderGroup);
Scrollview_wpanel6.addView(wpanel6);

return Scrollview_wpanel6;
}
public View getViewPage0View(final UMActivity context,IBinderGroup binderGroup) {
viewPage0 = (XVerticalLayout)ThirdControl.createControl(new XVerticalLayout(context),ID_VIEWPAGE0
,"background","#F5F5F5"
,"width","fill"
,"valign","TOP"
,"layout-type","vbox"
,"halign","center"
,"onload","action:viewpage0_onload"
,"height","fill"
);
View navigatorbar0 = (View) getNavigatorbar0View((UMActivity)context,binderGroup);
viewPage0.addView(navigatorbar0);
View Scrollview_wpanel6 = (View) getScrollview_wpanel6View((UMActivity)context,binderGroup);
viewPage0.addView(Scrollview_wpanel6);

return viewPage0;
}
public UMWindow getCurrentWindow(final UMActivity context,IBinderGroup binderGroup) {
Confirm = (UMWindow)ThirdControl.createControl(new UMWindow(context),ID_CONFIRM
,"layout","vbox"
,"controller","ConfirmController"
,"namespace","com.yonyou.dxyma"
,"width","fill"
,"valign","TOP"
,"onkeydown","action:confirm_onkeydown"
,"layout-type","linear"
,"halign","center"
,"height","fill"
);
View viewPage0 = (View) getViewPage0View((UMActivity)context,binderGroup);
Confirm.addView(viewPage0);

return (UMWindow)Confirm;
}

	
	public void actionButton2_onclick(View control, UMEventArgs args) {
    args.put("containerName","");
    args.put("language","javascript");
    String actionid = "button2_onclick";
  ActionProcessor.exec(this, actionid, args);
  this.getContainer().exec(actionid, "this.button2_onclick()",UMActivity.getViewId(control),args);
}
public void actionConfirm_onkeydown(View control, UMEventArgs args) {
    args.put("containerName","");
    args.put("language","javascript");
    String actionid = "confirm_onkeydown";
  ActionProcessor.exec(this, actionid, args);
  this.getContainer().exec(actionid, "this.onkeydown()",UMActivity.getViewId(control),args);
}
public void actionViewpage0_onload(View control, UMEventArgs args) {
    args.put("containerName","");
    args.put("language","javascript");
    String actionid = "viewpage0_onload";
  ActionProcessor.exec(this, actionid, args);
  this.getContainer().exec(actionid, "this.pageOnLoad()",UMActivity.getViewId(control),args);
}


}
