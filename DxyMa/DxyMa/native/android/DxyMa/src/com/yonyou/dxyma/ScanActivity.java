package com.yonyou.dxyma;

import com.yonyou.uap.um.application.ApplicationContext;
import com.yonyou.uap.um.base.*;
import com.yonyou.uap.um.common.*;
import com.yonyou.uap.um.third.*;
import com.yonyou.uap.um.control.*;
import com.yonyou.uap.um.core.*;
import com.yonyou.uap.um.binder.*;
import com.yonyou.uap.um.runtime.*;
import com.yonyou.uap.um.lexer.*;
import com.yonyou.uap.um.widget.*;
import com.yonyou.uap.um.widget.UmpSlidingLayout.SlidingViewType;
import com.yonyou.uap.um.log.ULog;
import java.util.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import android.webkit.*;
import android.content.*;
import android.graphics.*;
import android.widget.ImageView.ScaleType;

public abstract class ScanActivity extends UMWindowActivity {

	protected UMWindow Scan = null;
protected XVerticalLayout viewPage0 = null;
protected UMTwoDCode twodimensioncode0 = null;

	
	protected final static int ID_SCAN = 1382391267;
protected final static int ID_VIEWPAGE0 = 1712630797;
protected final static int ID_TWODIMENSIONCODE0 = 2007569813;

	
	
	@Override
	public String getControllerName() {
		return "ScanController";
	}
	
	@Override
	public String getContextName() {
		return "";
	}

	@Override
	public String getNameSpace() {
		return "com.yonyou.dxyma";
	}

	protected void onCreate(Bundle savedInstanceState) {
		ULog.logLC("onCreate", this);
		super.onCreate(savedInstanceState);
        onInit(savedInstanceState);
        	super.setEvent("onkeydown", "action:scan_onkeydown");

	}
	
	@Override
	protected void onStart() {
		ULog.logLC("onStart", this);
		
		super.onStart();
	}

	@Override
	protected void onRestart() {
		ULog.logLC("onRestart", this);
		
		super.onRestart();
	}

	@Override
	protected void onResume() {
		ULog.logLC("onResume", this);
		
		super.onResume();
	}

	@Override
	protected void onPause() {
		ULog.logLC("onPause", this);
		
		super.onPause();
	}

	@Override
	protected void onStop() {
		ULog.logLC("onStop", this);
		
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		ULog.logLC("onDestroy", this);
		
		super.onDestroy();
	}
	
	public  void onInit(Bundle savedInstanceState) {
		ULog.logLC("onInit", this);
		UMActivity context = this;
		registerControl();
		this.getContainer();
		
		/*
		 new Thread() {
			 public void run() {
				 UMPDebugClient.startServer();
			 }
		 }.start();
		*/
		String sys_debug = ApplicationContext.getCurrent(this).getValue("sys_debug");
		if (sys_debug != null && sys_debug.equalsIgnoreCase("true")) {
			UMPDebugClient.waitForDebug();
		}

		IBinderGroup binderGroup = this;
		currentPage = getCurrentWindow(context, binderGroup);
super.setContentView(currentPage);

		
	}
	
	private void registerControl() {
		  idmap.put("Scan",ID_SCAN);
  idmap.put("viewPage0",ID_VIEWPAGE0);
  idmap.put("twodimensioncode0",ID_TWODIMENSIONCODE0);

	}
	
	public void onLoad() {
		ULog.logLC("onLoad", this);
		if(currentPage!=null) {
			currentPage.onLoad();
		}
	
		{ //viewPage0 - action:viewpage0_onload
    UMEventArgs args = new UMEventArgs(ScanActivity.this);
    actionViewpage0_onload(viewPage0,args);

}

	}
	
	public void onDatabinding() {
		ULog.logLC("onDatabinding", this);
		super.onDatabinding();
		
	}
	
	@Override
	public void onReturn(String methodName, Object returnValue) {
		
	}

	@Override
	public void onAfterInit() {
		ULog.logLC("onAfterInit", this);
		
		onLoad();
	}
	
		@Override
	public Map<String,String> getPlugout(String id) {
		XJSON from = this.getUMContext();
		Map<String,String> r = super.getPlugout(id);
		
		return r;	
	}
	
	
	
	public View getViewPage0View(final UMActivity context,IBinderGroup binderGroup) {
viewPage0 = (XVerticalLayout)ThirdControl.createControl(new XVerticalLayout(context),ID_VIEWPAGE0
,"background","#F5F5F5"
,"width","fill"
,"valign","TOP"
,"layout-type","vbox"
,"halign","center"
,"onload","action:viewpage0_onload"
,"height","fill"
);
twodimensioncode0 = (UMTwoDCode)ThirdControl.createControl(new UMTwoDCode(context),ID_TWODIMENSIONCODE0
,"layout","relative"
,"bindfield","result"
,"width","fill"
,"onscansuccess","action:twodimensioncode0_onscansuccess"
,"layout-type","vbox"
,"height","fill"
);
viewPage0.addView(twodimensioncode0);

return viewPage0;
}
public UMWindow getCurrentWindow(final UMActivity context,IBinderGroup binderGroup) {
Scan = (UMWindow)ThirdControl.createControl(new UMWindow(context),ID_SCAN
,"layout","vbox"
,"controller","ScanController"
,"namespace","com.yonyou.dxyma"
,"width","fill"
,"valign","TOP"
,"onkeydown","action:scan_onkeydown"
,"layout-type","linear"
,"halign","center"
,"height","fill"
);
View viewPage0 = (View) getViewPage0View((UMActivity)context,binderGroup);
Scan.addView(viewPage0);

return (UMWindow)Scan;
}

	
	public void actionTwodimensioncode0_onscansuccess(View control, UMEventArgs args) {
    args.put("containerName","");
    args.put("language","javascript");
    String actionid = "twodimensioncode0_onscansuccess";
  ActionProcessor.exec(this, actionid, args);
  this.getContainer().exec(actionid, "this.twodimensioncode0_onscansuccess()",UMActivity.getViewId(control),args);
}
public void actionViewpage0_onload(View control, UMEventArgs args) {
    args.put("containerName","");
    args.put("language","javascript");
    String actionid = "viewpage0_onload";
  ActionProcessor.exec(this, actionid, args);
  this.getContainer().exec(actionid, "this.viewPage0_onload()",UMActivity.getViewId(control),args);
}
public void actionScan_onkeydown(View control, UMEventArgs args) {
    args.put("containerName","");
    args.put("language","javascript");
    String actionid = "scan_onkeydown";
  ActionProcessor.exec(this, actionid, args);
  this.getContainer().exec(actionid, "this.onkeydown()",UMActivity.getViewId(control),args);
}


}
