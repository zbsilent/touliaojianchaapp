package com.yonyou.dxyma;

import com.yonyou.uap.um.application.ApplicationContext;
import com.yonyou.uap.um.base.*;
import com.yonyou.uap.um.common.*;
import com.yonyou.uap.um.third.*;
import com.yonyou.uap.um.control.*;
import com.yonyou.uap.um.core.*;
import com.yonyou.uap.um.binder.*;
import com.yonyou.uap.um.runtime.*;
import com.yonyou.uap.um.lexer.*;
import com.yonyou.uap.um.widget.*;
import com.yonyou.uap.um.widget.UmpSlidingLayout.SlidingViewType;
import com.yonyou.uap.um.log.ULog;
import java.util.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import android.webkit.*;
import android.content.*;
import android.graphics.*;
import android.widget.ImageView.ScaleType;

public abstract class MachineLoginActivity extends UMWindowActivity {

	protected UMWindow MachineLogin = null;
protected XVerticalLayout viewPage0 = null;
protected XHorizontalLayout panel0 = null;
protected UMImageButton imagebutton0 = null;
protected UMLabel label0 = null;
protected UMImageFlipper imageflipper0 = null;
protected UMImageFlipperItem imageflipper0_0 = null;
protected UMImageFlipperItem imageflipper0_1 = null;
protected XHorizontalLayout panel1 = null;
protected UMLabel label1 = null;
protected UMLabel label2 = null;
protected UMCustomPickerLayout picker0 = null;
protected UMCustomPickerItem picker0_0 = null;
protected UMButton button0 = null;

	
	protected final static int ID_MACHINELOGIN = 1728300318;
protected final static int ID_VIEWPAGE0 = 171140844;
protected final static int ID_PANEL0 = 1771151830;
protected final static int ID_IMAGEBUTTON0 = 1021635255;
protected final static int ID_LABEL0 = 717408905;
protected final static int ID_IMAGEFLIPPER0 = 1720575480;
protected final static int ID_IMAGEFLIPPER0_0 = 1076477191;
protected final static int ID_IMAGEFLIPPER0_1 = 1895761805;
protected final static int ID_PANEL1 = 758953302;
protected final static int ID_LABEL1 = 1737690860;
protected final static int ID_LABEL2 = 1659277672;
protected final static int ID_PICKER0 = 1384451977;
protected final static int ID_PICKER0_0 = 1018609358;
protected final static int ID_BUTTON0 = 1193710353;

	
	
	@Override
	public String getControllerName() {
		return "MachineLoginController";
	}
	
	@Override
	public String getContextName() {
		return "";
	}

	@Override
	public String getNameSpace() {
		return "com.yonyou.dxyma";
	}

	protected void onCreate(Bundle savedInstanceState) {
		ULog.logLC("onCreate", this);
		super.onCreate(savedInstanceState);
        onInit(savedInstanceState);
        
	}
	
	@Override
	protected void onStart() {
		ULog.logLC("onStart", this);
		
		super.onStart();
	}

	@Override
	protected void onRestart() {
		ULog.logLC("onRestart", this);
		
		super.onRestart();
	}

	@Override
	protected void onResume() {
		ULog.logLC("onResume", this);
		
		super.onResume();
	}

	@Override
	protected void onPause() {
		ULog.logLC("onPause", this);
		
		super.onPause();
	}

	@Override
	protected void onStop() {
		ULog.logLC("onStop", this);
		
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		ULog.logLC("onDestroy", this);
		
		super.onDestroy();
	}
	
	public  void onInit(Bundle savedInstanceState) {
		ULog.logLC("onInit", this);
		UMActivity context = this;
		registerControl();
		this.getContainer();
		
		/*
		 new Thread() {
			 public void run() {
				 UMPDebugClient.startServer();
			 }
		 }.start();
		*/
		String sys_debug = ApplicationContext.getCurrent(this).getValue("sys_debug");
		if (sys_debug != null && sys_debug.equalsIgnoreCase("true")) {
			UMPDebugClient.waitForDebug();
		}

		IBinderGroup binderGroup = this;
		currentPage = getCurrentWindow(context, binderGroup);
super.setContentView(currentPage);

		
	}
	
	private void registerControl() {
		  idmap.put("MachineLogin",ID_MACHINELOGIN);
  idmap.put("viewPage0",ID_VIEWPAGE0);
  idmap.put("panel0",ID_PANEL0);
  idmap.put("imagebutton0",ID_IMAGEBUTTON0);
  idmap.put("label0",ID_LABEL0);
  idmap.put("imageflipper0",ID_IMAGEFLIPPER0);
  idmap.put("imageflipper0_0",ID_IMAGEFLIPPER0_0);
  idmap.put("imageflipper0_1",ID_IMAGEFLIPPER0_1);
  idmap.put("panel1",ID_PANEL1);
  idmap.put("label1",ID_LABEL1);
  idmap.put("label2",ID_LABEL2);
  idmap.put("picker0",ID_PICKER0);
  idmap.put("picker0_0",ID_PICKER0_0);
  idmap.put("button0",ID_BUTTON0);

	}
	
	public void onLoad() {
		ULog.logLC("onLoad", this);
		if(currentPage!=null) {
			currentPage.onLoad();
		}
	
		{ //viewPage0 - action:viewpage0_onload
    UMEventArgs args = new UMEventArgs(MachineLoginActivity.this);
    actionViewpage0_onload(viewPage0,args);

}

	}
	
	public void onDatabinding() {
		ULog.logLC("onDatabinding", this);
		super.onDatabinding();
		
	}
	
	@Override
	public void onReturn(String methodName, Object returnValue) {
		
	}

	@Override
	public void onAfterInit() {
		ULog.logLC("onAfterInit", this);
		
		onLoad();
	}
	
		@Override
	public Map<String,String> getPlugout(String id) {
		XJSON from = this.getUMContext();
		Map<String,String> r = super.getPlugout(id);
		
		return r;	
	}
	
	
	
	public View getPanel0View(final UMActivity context,IBinderGroup binderGroup) {
panel0 = (XHorizontalLayout)ThirdControl.createControl(new XHorizontalLayout(context),ID_PANEL0
,"halign","LEFT"
,"height","50"
,"layout-type","vbox"
,"width","fill"
,"valign","center"
);
imagebutton0 = (UMImageButton)ThirdControl.createControl(new UMImageButton(context),ID_IMAGEBUTTON0
,"halign","center"
,"width","64"
,"icon-width","25"
,"istogglebutton","false"
,"font-pressed-color","#e50011"
,"imagebuttontype","icon"
,"icon-height","25"
,"height","49"
,"color","#919191"
,"layout-type","hbox"
,"font-size","10"
,"icon-background-image","return.png"
,"value","图标名称"
,"icon-pressed-image","returntouch.png"
,"font-family","default"
,"valign","center"
,"checked","false"
,"icon-text-spacing","3"
);
panel0.addView(imagebutton0);
label0 = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_LABEL0
,"content","机台指令登录"
,"margin-right","30"
,"halign","center"
,"height","50"
,"color","#000000"
,"layout-type","hbox"
,"font-size","30"
,"width","fill"
,"font-family","default"
,"valign","center"
);
panel0.addView(label0);

return panel0;
}
public View getImageflipper0View(final UMActivity context,IBinderGroup binderGroup) {
imageflipper0 = (UMImageFlipper)ThirdControl.createControl(new UMImageFlipper(context),ID_IMAGEFLIPPER0
,"titleheight","20"
,"height","150"
,"descheight","0"
,"interval","2000"
,"flipperbtnvisible","false"
,"layout-type","vbox"
,"width","fill"
,"scaletype","fitcenter"
,"isloop","true"
,"autoflip","true"
);
imageflipper0_0 = (UMImageFlipperItem)ThirdControl.createControl(new UMImageFlipperItem(context),ID_IMAGEFLIPPER0_0
,"title","用友软件"
,"description","1"
,"layout-type","vbox"
,"src","index.jpg"
);
imageflipper0.addView(imageflipper0_0);
imageflipper0_1 = (UMImageFlipperItem)ThirdControl.createControl(new UMImageFlipperItem(context),ID_IMAGEFLIPPER0_1
,"title","大西洋"
,"description","2"
,"layout-type","vbox"
,"src","dxy.jpg"
);
imageflipper0.addView(imageflipper0_1);

return imageflipper0;
}
public View getPicker0View(final UMActivity context,IBinderGroup binderGroup) {
picker0 = (UMCustomPickerLayout)ThirdControl.createControl(new UMCustomPickerLayout(context),ID_PICKER0
,"height","fill"
,"showsselectionindicator","true"
,"layout-type","hbox"
,"width","fill"
);
picker0_0 = (UMCustomPickerItem)ThirdControl.createControl(new UMCustomPickerItem(context),ID_PICKER0_0
,"bindfield","machine_code"
,"onselectedchange","action:picker0_0_onselectedchange"
,"layout-type","hbox"
,"value","机台指令"
,"datasource","machine_codes"
);
picker0.addView(picker0_0);

return picker0;
}
public View getPanel1View(final UMActivity context,IBinderGroup binderGroup) {
panel1 = (XHorizontalLayout)ThirdControl.createControl(new XHorizontalLayout(context),ID_PANEL1
,"padding-right","10"
,"border-bottom-width","1"
,"halign","CENTER"
,"widthwrap","375.0"
,"height","wrap"
,"heightwrap","10.0"
,"width","wrap"
,"layout-type","vbox"
,"background","#ffffff"
,"margin-top","20"
,"valign","center"
,"border-bottom-color","#e2e2e2"
);
label1 = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_LABEL1
,"content","*"
,"halign","center"
,"height","10"
,"visible","false"
,"color","#e50011"
,"layout-type","hbox"
,"font-size","14"
,"width","10"
,"font-family","default"
,"valign","center"
);
panel1.addView(label1);
label2 = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_LABEL2
,"halign","left"
,"widthwrap","32.0"
,"width","wrap"
,"content","机台"
,"margin-right","4"
,"height","wrap"
,"color","#333333"
,"heightwrap","10.0"
,"font-size","15"
,"layout-type","hbox"
,"font-family","default"
,"valign","center"
);
panel1.addView(label2);
View picker0 = (View) getPicker0View((UMActivity)context,binderGroup);
panel1.addView(picker0);

return panel1;
}
public View getViewPage0View(final UMActivity context,IBinderGroup binderGroup) {
viewPage0 = (XVerticalLayout)ThirdControl.createControl(new XVerticalLayout(context),ID_VIEWPAGE0
,"halign","center"
,"height","fill"
,"onload","action:viewpage0_onload"
,"layout-type","vbox"
,"background","#F5F5F5"
,"width","fill"
,"valign","TOP"
);
View panel0 = (View) getPanel0View((UMActivity)context,binderGroup);
viewPage0.addView(panel0);
View imageflipper0 = (View) getImageflipper0View((UMActivity)context,binderGroup);
viewPage0.addView(imageflipper0);
View panel1 = (View) getPanel1View((UMActivity)context,binderGroup);
viewPage0.addView(panel1);
button0 = (UMButton)ThirdControl.createControl(new UMButton(context),ID_BUTTON0
,"halign","center"
,"width","fill"
,"font-pressed-color","#f2adb2"
,"height","44"
,"color","#e50011"
,"layout-type","vbox"
,"font-size","17"
,"background","#ff8040"
,"value","登录"
,"onclick","action:button0_onclick"
,"font-family","default"
,"margin-top","20"
,"valign","center"
);
viewPage0.addView(button0);

return viewPage0;
}
public UMWindow getCurrentWindow(final UMActivity context,IBinderGroup binderGroup) {
MachineLogin = (UMWindow)ThirdControl.createControl(new UMWindow(context),ID_MACHINELOGIN
,"halign","center"
,"height","fill"
,"layout-type","linear"
,"layout","vbox"
,"width","fill"
,"controller","MachineLoginController"
,"valign","TOP"
,"namespace","com.yonyou.dxyma"
);
View viewPage0 = (View) getViewPage0View((UMActivity)context,binderGroup);
MachineLogin.addView(viewPage0);

return (UMWindow)MachineLogin;
}

	
	public void actionButton0_onclick(View control, UMEventArgs args) {
    String actionid = "button0_onclick";
    args.put("language","javascript");
    args.put("containerName","");
  ActionProcessor.exec(this, actionid, args);
  this.getContainer().exec(actionid, "this.button0_onclick()",UMActivity.getViewId(control),args);
}
public void actionViewpage0_onload(View control, UMEventArgs args) {
    String actionid = "viewpage0_onload";
    args.put("language","javascript");
    args.put("containerName","");
  ActionProcessor.exec(this, actionid, args);
  this.getContainer().exec(actionid, "this.onPageLoad()",UMActivity.getViewId(control),args);
}
public void actionPicker0_0_onselectedchange(View control, UMEventArgs args) {
    String actionid = "picker0_0_onselectedchange";
    args.put("language","javascript");
    args.put("containerName","");
  ActionProcessor.exec(this, actionid, args);
  this.getContainer().exec(actionid, "onChange()",UMActivity.getViewId(control),args);
}


}
