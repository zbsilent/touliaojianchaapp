package com.yonyou.dxyma;

import com.yonyou.uap.um.application.ApplicationContext;
import com.yonyou.uap.um.base.*;
import com.yonyou.uap.um.common.*;
import com.yonyou.uap.um.third.*;
import com.yonyou.uap.um.control.*;
import com.yonyou.uap.um.core.*;
import com.yonyou.uap.um.binder.*;
import com.yonyou.uap.um.runtime.*;
import com.yonyou.uap.um.lexer.*;
import com.yonyou.uap.um.widget.*;
import com.yonyou.uap.um.widget.UmpSlidingLayout.SlidingViewType;
import com.yonyou.uap.um.log.ULog;
import java.util.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import android.webkit.*;
import android.content.*;
import android.graphics.*;
import android.widget.ImageView.ScaleType;

public abstract class ConfigActivity extends UMWindowActivity {

	protected UMWindow Config = null;
protected XVerticalLayout viewPage0 = null;
protected XHorizontalLayout panel0 = null;
protected UMImageButton imagebutton0 = null;
protected UMLabel label0 = null;
protected UMLabel label1 = null;
protected XVerticalLayout panel1 = null;
protected UMImage image0 = null;
protected UMLabel label2 = null;
protected XHorizontalLayout panel2 = null;
protected UMLabel label3 = null;
protected UMTextbox textbox0 = null;
protected UMLabel label4 = null;
protected XHorizontalLayout panel3 = null;
protected UMLabel label5 = null;
protected UMTextbox textbox1 = null;
protected UMButton button0 = null;

	
	protected final static int ID_CONFIG = 1434723344;
protected final static int ID_VIEWPAGE0 = 1814752695;
protected final static int ID_PANEL0 = 2048954932;
protected final static int ID_IMAGEBUTTON0 = 1760366489;
protected final static int ID_LABEL0 = 1805392478;
protected final static int ID_LABEL1 = 2058780267;
protected final static int ID_PANEL1 = 1527911926;
protected final static int ID_IMAGE0 = 113539822;
protected final static int ID_LABEL2 = 1168419467;
protected final static int ID_PANEL2 = 307745485;
protected final static int ID_LABEL3 = 1183757960;
protected final static int ID_TEXTBOX0 = 281932812;
protected final static int ID_LABEL4 = 799739635;
protected final static int ID_PANEL3 = 1680884895;
protected final static int ID_LABEL5 = 1859285655;
protected final static int ID_TEXTBOX1 = 497604060;
protected final static int ID_BUTTON0 = 203408171;

	
	
	@Override
	public String getControllerName() {
		return "ConfigController";
	}
	
	@Override
	public String getContextName() {
		return "";
	}

	@Override
	public String getNameSpace() {
		return "com.yonyou.dxyma";
	}

	protected void onCreate(Bundle savedInstanceState) {
		ULog.logLC("onCreate", this);
		super.onCreate(savedInstanceState);
        onInit(savedInstanceState);
        	super.setEvent("onload", "action:config_onload");
	super.setEvent("onkeydown", "action:config_onkeydown");

	}
	
	@Override
	protected void onStart() {
		ULog.logLC("onStart", this);
		
		super.onStart();
	}

	@Override
	protected void onRestart() {
		ULog.logLC("onRestart", this);
		
		super.onRestart();
	}

	@Override
	protected void onResume() {
		ULog.logLC("onResume", this);
		
		super.onResume();
	}

	@Override
	protected void onPause() {
		ULog.logLC("onPause", this);
		
		super.onPause();
	}

	@Override
	protected void onStop() {
		ULog.logLC("onStop", this);
		
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		ULog.logLC("onDestroy", this);
		
		super.onDestroy();
	}
	
	public  void onInit(Bundle savedInstanceState) {
		ULog.logLC("onInit", this);
		UMActivity context = this;
		registerControl();
		this.getContainer();
		
		/*
		 new Thread() {
			 public void run() {
				 UMPDebugClient.startServer();
			 }
		 }.start();
		*/
		String sys_debug = ApplicationContext.getCurrent(this).getValue("sys_debug");
		if (sys_debug != null && sys_debug.equalsIgnoreCase("true")) {
			UMPDebugClient.waitForDebug();
		}

		IBinderGroup binderGroup = this;
		currentPage = getCurrentWindow(context, binderGroup);
super.setContentView(currentPage);

		
	}
	
	private void registerControl() {
		  idmap.put("Config",ID_CONFIG);
  idmap.put("viewPage0",ID_VIEWPAGE0);
  idmap.put("panel0",ID_PANEL0);
  idmap.put("imagebutton0",ID_IMAGEBUTTON0);
  idmap.put("label0",ID_LABEL0);
  idmap.put("label1",ID_LABEL1);
  idmap.put("panel1",ID_PANEL1);
  idmap.put("image0",ID_IMAGE0);
  idmap.put("label2",ID_LABEL2);
  idmap.put("panel2",ID_PANEL2);
  idmap.put("label3",ID_LABEL3);
  idmap.put("textbox0",ID_TEXTBOX0);
  idmap.put("label4",ID_LABEL4);
  idmap.put("panel3",ID_PANEL3);
  idmap.put("label5",ID_LABEL5);
  idmap.put("textbox1",ID_TEXTBOX1);
  idmap.put("button0",ID_BUTTON0);

	}
	
	public void onLoad() {
		ULog.logLC("onLoad", this);
		if(currentPage!=null) {
			currentPage.onLoad();
		}
	
		{ //viewPage0 - action:viewpage0_onload
    UMEventArgs args = new UMEventArgs(ConfigActivity.this);
    actionViewpage0_onload(viewPage0,args);

}
{ //panel1 - action:panel1_onload
    UMEventArgs args = new UMEventArgs(ConfigActivity.this);
    actionPanel1_onload(panel1,args);

}

	}
	
	public void onDatabinding() {
		ULog.logLC("onDatabinding", this);
		super.onDatabinding();
		
	}
	
	@Override
	public void onReturn(String methodName, Object returnValue) {
		
	}

	@Override
	public void onAfterInit() {
		ULog.logLC("onAfterInit", this);
		
		onLoad();
	}
	
		@Override
	public Map<String,String> getPlugout(String id) {
		XJSON from = this.getUMContext();
		Map<String,String> r = super.getPlugout(id);
		
		return r;	
	}
	
	
	
	public View getPanel0View(final UMActivity context,IBinderGroup binderGroup) {
panel0 = (XHorizontalLayout)ThirdControl.createControl(new XHorizontalLayout(context),ID_PANEL0
,"width","fill"
,"valign","center"
,"layout-type","vbox"
,"halign","LEFT"
,"height","50"
);
imagebutton0 = (UMImageButton)ThirdControl.createControl(new UMImageButton(context),ID_IMAGEBUTTON0
,"font-pressed-color","#e50011"
,"color","#919191"
,"icon-pressed-image","button_image_touch"
,"icon-height","25"
,"font-size","10"
,"valign","center"
,"icon-text-spacing","3"
,"imagebuttontype","icon"
,"icon-background-image","return.png"
,"istogglebutton","false"
,"width","50"
,"checked","false"
,"font-family","default"
,"layout-type","hbox"
,"halign","center"
,"icon-width","25"
,"value","图标名称"
,"height","50"
);
panel0.addView(imagebutton0);
label0 = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_LABEL0
,"color","#000000"
,"font-weight","bold"
,"onclick","action:label0_onclick"
,"weight","1"
,"font-size","30"
,"valign","center"
,"content","设置"
,"width","0"
,"font-family","default"
,"layout-type","hbox"
,"halign","center"
,"height","50"
);
panel0.addView(label0);
label1 = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_LABEL1
,"color","#000000"
,"heightwrap","20.0"
,"onclick","action:label1_onclick"
,"width","wrap"
,"font-size","14"
,"valign","center"
,"font-family","default"
,"layout-type","hbox"
,"halign","center"
,"widthwrap","34.0"
,"height","wrap"
);
panel0.addView(label1);

return panel0;
}
public View getPanel2View(final UMActivity context,IBinderGroup binderGroup) {
panel2 = (XHorizontalLayout)ThirdControl.createControl(new XHorizontalLayout(context),ID_PANEL2
,"margin-left","10"
,"width","fill"
,"valign","center"
,"layout-type","vbox"
,"halign","LEFT"
,"margin-right","10"
,"height","40"
);
label3 = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_LABEL3
,"margin-left","10"
,"color","#000000"
,"width","60"
,"font-size","14"
,"valign","center"
,"font-family","default"
,"layout-type","hbox"
,"halign","center"
,"content","IP地址："
,"height","40"
);
panel2.addView(label3);
textbox0 = (UMTextbox)ThirdControl.createControl(new UMTextbox(context),ID_TEXTBOX0
,"color","#167ef8"
,"maxlength","256"
,"padding-left","4"
,"font-size","17"
,"margin-right","20"
,"background","#ffffff"
,"width","285"
,"font-family","default"
,"placeholder",""
,"layout-type","hbox"
,"halign","LEFT"
,"height","40.0"
);
panel2.addView(textbox0);

return panel2;
}
public View getPanel3View(final UMActivity context,IBinderGroup binderGroup) {
panel3 = (XHorizontalLayout)ThirdControl.createControl(new XHorizontalLayout(context),ID_PANEL3
,"margin-left","10"
,"width","fill"
,"valign","center"
,"layout-type","vbox"
,"halign","LEFT"
,"margin-right","10"
,"height","40"
);
label5 = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_LABEL5
,"margin-left","10"
,"color","#000000"
,"onclick","action:label5_onclick"
,"width","60"
,"font-size","14"
,"valign","center"
,"font-family","default"
,"layout-type","hbox"
,"halign","center"
,"content","端口："
,"height","40"
);
panel3.addView(label5);
textbox1 = (UMTextbox)ThirdControl.createControl(new UMTextbox(context),ID_TEXTBOX1
,"color","#167ef8"
,"maxlength","256"
,"padding-left","4"
,"font-size","17"
,"margin-right","10"
,"background","#ffffff"
,"width","285.0"
,"font-family","default"
,"placeholder",""
,"layout-type","hbox"
,"halign","LEFT"
,"height","44"
);
panel3.addView(textbox1);

return panel3;
}
public View getPanel1View(final UMActivity context,IBinderGroup binderGroup) {
panel1 = (XVerticalLayout)ThirdControl.createControl(new XVerticalLayout(context),ID_PANEL1
,"width","fill"
,"valign","TOP"
,"layout-type","vbox"
,"halign","center"
,"onload","action:panel1_onload"
,"height","fill"
);
image0 = (UMImage)ThirdControl.createControl(new UMImage(context),ID_IMAGE0
,"src","index.jpg"
,"onclick","action:image0_onclick"
,"width","fill"
,"scaletype","fitcenter"
,"layout-type","vbox"
,"height","80"
);
panel1.addView(image0);
label2 = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_LABEL2
,"color","#000000"
,"width","fill"
,"font-size","14"
,"valign","center"
,"font-family","default"
,"layout-type","vbox"
,"halign","center"
,"height","20"
);
panel1.addView(label2);
View panel2 = (View) getPanel2View((UMActivity)context,binderGroup);
panel1.addView(panel2);
label4 = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_LABEL4
,"color","#000000"
,"width","fill"
,"font-size","14"
,"valign","center"
,"font-family","default"
,"layout-type","vbox"
,"halign","center"
,"height","20"
);
panel1.addView(label4);
View panel3 = (View) getPanel3View((UMActivity)context,binderGroup);
panel1.addView(panel3);
button0 = (UMButton)ThirdControl.createControl(new UMButton(context),ID_BUTTON0
,"font-pressed-color","#000000"
,"color","#ffffff"
,"onclick","action:button0_onclick"
,"font-size","25"
,"valign","center"
,"margin-right","10"
,"margin-left","10"
,"background","#ff8040"
,"width","fill"
,"font-family","default"
,"margin-top","30"
,"layout-type","vbox"
,"halign","center"
,"value","注册"
,"height","44"
);
panel1.addView(button0);

return panel1;
}
public View getViewPage0View(final UMActivity context,IBinderGroup binderGroup) {
viewPage0 = (XVerticalLayout)ThirdControl.createControl(new XVerticalLayout(context),ID_VIEWPAGE0
,"background","#F7F8F8"
,"width","fill"
,"valign","TOP"
,"layout-type","vbox"
,"halign","center"
,"onload","action:viewpage0_onload"
,"height","fill"
);
View panel0 = (View) getPanel0View((UMActivity)context,binderGroup);
viewPage0.addView(panel0);
View panel1 = (View) getPanel1View((UMActivity)context,binderGroup);
viewPage0.addView(panel1);

return viewPage0;
}
public UMWindow getCurrentWindow(final UMActivity context,IBinderGroup binderGroup) {
Config = (UMWindow)ThirdControl.createControl(new UMWindow(context),ID_CONFIG
,"canvaswidth","375"
,"orientation","vertical"
,"controller","ConfigController"
,"valign","TOP"
,"onload","action:config_onload"
,"layout","vbox"
,"canvasheight","667"
,"namespace","com.yonyou.dxyma"
,"width","fill"
,"onkeydown","action:config_onkeydown"
,"layout-type","linear"
,"halign","center"
,"height","fill"
);
View viewPage0 = (View) getViewPage0View((UMActivity)context,binderGroup);
Config.addView(viewPage0);

return (UMWindow)Config;
}

	
	public void actionLabel1_onclick(View control, UMEventArgs args) {
    args.put("containerName","");
    args.put("language","javascript");
    String actionid = "label1_onclick";
  ActionProcessor.exec(this, actionid, args);
  this.getContainer().exec(actionid, "this.label1_onclick()",UMActivity.getViewId(control),args);
}
public void actionPanel1_onload(View control, UMEventArgs args) {
    args.put("containerName","");
    args.put("language","javascript");
    String actionid = "panel1_onload";
  ActionProcessor.exec(this, actionid, args);
  this.getContainer().exec(actionid, "this.panel1_onload()",UMActivity.getViewId(control),args);
}
public void actionLabel0_onclick(View control, UMEventArgs args) {
    args.put("containerName","");
    args.put("language","javascript");
    String actionid = "label0_onclick";
  ActionProcessor.exec(this, actionid, args);
  this.getContainer().exec(actionid, "this.label0_onclick()",UMActivity.getViewId(control),args);
}
public void actionViewpage0_onload(View control, UMEventArgs args) {
    args.put("containerName","");
    args.put("language","javascript");
    String actionid = "viewpage0_onload";
  ActionProcessor.exec(this, actionid, args);
  this.getContainer().exec(actionid, "this.pageOnLoad()",UMActivity.getViewId(control),args);
}
public void actionLabel5_onclick(View control, UMEventArgs args) {
    args.put("containerName","");
    args.put("language","javascript");
    String actionid = "label5_onclick";
  ActionProcessor.exec(this, actionid, args);
  this.getContainer().exec(actionid, "this.label5_onclick()",UMActivity.getViewId(control),args);
}
public void actionButton0_onclick(View control, UMEventArgs args) {
    args.put("containerName","");
    args.put("language","javascript");
    String actionid = "button0_onclick";
  ActionProcessor.exec(this, actionid, args);
  this.getContainer().exec(actionid, "this.onRegButClick()",UMActivity.getViewId(control),args);
}
public void actionConfig_onkeydown(View control, UMEventArgs args) {
    args.put("containerName","");
    args.put("language","javascript");
    String actionid = "config_onkeydown";
  ActionProcessor.exec(this, actionid, args);
  this.getContainer().exec(actionid, "this.onkeydown()",UMActivity.getViewId(control),args);
}
public void actionConfig_onload(View control, UMEventArgs args) {
    args.put("containerName","");
    args.put("language","javascript");
    String actionid = "config_onload";
  ActionProcessor.exec(this, actionid, args);
  this.getContainer().exec(actionid, "this.windowOnLoad()",UMActivity.getViewId(control),args);
}
public void actionImage0_onclick(View control, UMEventArgs args) {
    args.put("containerName","");
    args.put("language","javascript");
    String actionid = "image0_onclick";
  ActionProcessor.exec(this, actionid, args);
  this.getContainer().exec(actionid, "this.image0_onclick()",UMActivity.getViewId(control),args);
}


}
