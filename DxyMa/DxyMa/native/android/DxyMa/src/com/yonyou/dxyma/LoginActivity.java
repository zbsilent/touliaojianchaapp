package com.yonyou.dxyma;

import com.yonyou.uap.um.application.ApplicationContext;
import com.yonyou.uap.um.base.*;
import com.yonyou.uap.um.common.*;
import com.yonyou.uap.um.third.*;
import com.yonyou.uap.um.control.*;
import com.yonyou.uap.um.core.*;
import com.yonyou.uap.um.binder.*;
import com.yonyou.uap.um.runtime.*;
import com.yonyou.uap.um.lexer.*;
import com.yonyou.uap.um.widget.*;
import com.yonyou.uap.um.widget.UmpSlidingLayout.SlidingViewType;
import com.yonyou.uap.um.log.ULog;
import java.util.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import android.webkit.*;
import android.content.*;
import android.graphics.*;
import android.widget.ImageView.ScaleType;

public abstract class LoginActivity extends UMWindowActivity {

	protected UMWindow Login = null;
protected XVerticalLayout viewPage0 = null;
protected XHorizontalLayout navigatorbar0 = null;
protected UMButton button0 = null;
protected UMLabel label0 = null;
protected UMButton button1 = null;
protected XVerticalLayout wloginpanel = null;
protected XHorizontalLayout wuserpanel = null;
protected UMImage wuserimg = null;
protected UMTextbox wusertext = null;
protected XHorizontalLayout wpasspanel = null;
protected UMImage wpassimg = null;
protected UMPassword wpasstext = null;
protected XVerticalLayout wforgetpasspanel = null;
protected UMButton wloginbutton = null;
protected XHorizontalLayout wotherpanel = null;

	
	protected final static int ID_LOGIN = 1982295269;
protected final static int ID_VIEWPAGE0 = 1838879288;
protected final static int ID_NAVIGATORBAR0 = 1764101732;
protected final static int ID_BUTTON0 = 968989517;
protected final static int ID_LABEL0 = 656929448;
protected final static int ID_BUTTON1 = 1071695770;
protected final static int ID_WLOGINPANEL = 57740201;
protected final static int ID_WUSERPANEL = 1728254735;
protected final static int ID_WUSERIMG = 1845499305;
protected final static int ID_WUSERTEXT = 156161863;
protected final static int ID_WPASSPANEL = 556104906;
protected final static int ID_WPASSIMG = 1703012992;
protected final static int ID_WPASSTEXT = 1870754566;
protected final static int ID_WFORGETPASSPANEL = 1494691098;
protected final static int ID_WLOGINBUTTON = 344339245;
protected final static int ID_WOTHERPANEL = 1233947225;

	
	
	@Override
	public String getControllerName() {
		return "LoginController";
	}
	
	@Override
	public String getContextName() {
		return "";
	}

	@Override
	public String getNameSpace() {
		return "com.yonyou.dxyma";
	}

	protected void onCreate(Bundle savedInstanceState) {
		ULog.logLC("onCreate", this);
		super.onCreate(savedInstanceState);
        onInit(savedInstanceState);
        	super.setEvent("onkeydown", "action:login_onkeydown");

	}
	
	@Override
	protected void onStart() {
		ULog.logLC("onStart", this);
		
		super.onStart();
	}

	@Override
	protected void onRestart() {
		ULog.logLC("onRestart", this);
		
		super.onRestart();
	}

	@Override
	protected void onResume() {
		ULog.logLC("onResume", this);
		
		super.onResume();
	}

	@Override
	protected void onPause() {
		ULog.logLC("onPause", this);
		
		super.onPause();
	}

	@Override
	protected void onStop() {
		ULog.logLC("onStop", this);
		
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		ULog.logLC("onDestroy", this);
		
		super.onDestroy();
	}
	
	public  void onInit(Bundle savedInstanceState) {
		ULog.logLC("onInit", this);
		UMActivity context = this;
		registerControl();
		this.getContainer();
		
		/*
		 new Thread() {
			 public void run() {
				 UMPDebugClient.startServer();
			 }
		 }.start();
		*/
		String sys_debug = ApplicationContext.getCurrent(this).getValue("sys_debug");
		if (sys_debug != null && sys_debug.equalsIgnoreCase("true")) {
			UMPDebugClient.waitForDebug();
		}

		IBinderGroup binderGroup = this;
		currentPage = getCurrentWindow(context, binderGroup);
super.setContentView(currentPage);

		
	}
	
	private void registerControl() {
		  idmap.put("Login",ID_LOGIN);
  idmap.put("viewPage0",ID_VIEWPAGE0);
  idmap.put("navigatorbar0",ID_NAVIGATORBAR0);
  idmap.put("button0",ID_BUTTON0);
  idmap.put("label0",ID_LABEL0);
  idmap.put("button1",ID_BUTTON1);
  idmap.put("wloginpanel",ID_WLOGINPANEL);
  idmap.put("wuserpanel",ID_WUSERPANEL);
  idmap.put("wuserimg",ID_WUSERIMG);
  idmap.put("wusertext",ID_WUSERTEXT);
  idmap.put("wpasspanel",ID_WPASSPANEL);
  idmap.put("wpassimg",ID_WPASSIMG);
  idmap.put("wpasstext",ID_WPASSTEXT);
  idmap.put("wforgetpasspanel",ID_WFORGETPASSPANEL);
  idmap.put("wloginbutton",ID_WLOGINBUTTON);
  idmap.put("wotherpanel",ID_WOTHERPANEL);

	}
	
	public void onLoad() {
		ULog.logLC("onLoad", this);
		if(currentPage!=null) {
			currentPage.onLoad();
		}
	
		{ //wloginpanel - action:wloginpanel_onload
    UMEventArgs args = new UMEventArgs(LoginActivity.this);
    actionWloginpanel_onload(wloginpanel,args);

}

	}
	
	public void onDatabinding() {
		ULog.logLC("onDatabinding", this);
		super.onDatabinding();
		
	}
	
	@Override
	public void onReturn(String methodName, Object returnValue) {
		
	}

	@Override
	public void onAfterInit() {
		ULog.logLC("onAfterInit", this);
		
		onLoad();
	}
	
		@Override
	public Map<String,String> getPlugout(String id) {
		XJSON from = this.getUMContext();
		Map<String,String> r = super.getPlugout(id);
		
		return r;	
	}
	
	
	
	public View getNavigatorbar0View(final UMActivity context,IBinderGroup binderGroup) {
navigatorbar0 = (XHorizontalLayout)ThirdControl.createControl(new XHorizontalLayout(context),ID_NAVIGATORBAR0
,"color","#323232"
,"padding-left","8"
,"font-size","17"
,"valign","center"
,"title","登陆界面"
,"width","fill"
,"font-family","default"
,"background-image","navbar_login.png"
,"layout-type","vbox"
,"halign","LEFT"
,"padding-right","8"
,"height","44.0"
);
button0 = (UMButton)ThirdControl.createControl(new UMButton(context),ID_BUTTON0
,"font-pressed-color","#e50011"
,"color","#ffffff"
,"width","44"
,"font-size","20"
,"font-family","default"
,"valign","center"
,"background-image","icon_back.png"
,"layout-type","hbox"
,"halign","center"
,"height","44"
);
navigatorbar0.addView(button0);
label0 = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_LABEL0
,"width","0"
,"weight","1"
,"valign","center"
,"font-family","default"
,"layout-type","hbox"
,"halign","center"
,"height","fill"
);
navigatorbar0.addView(label0);
button1 = (UMButton)ThirdControl.createControl(new UMButton(context),ID_BUTTON1
,"font-pressed-color","#e50011"
,"color","#ffffff"
,"onclick","action:button1_onclick"
,"width","44.0"
,"font-size","20"
,"font-family","default"
,"valign","center"
,"background-image","icon_disk.png"
,"layout-type","hbox"
,"halign","center"
,"height","44"
);
navigatorbar0.addView(button1);

return navigatorbar0;
}
public View getWuserpanelView(final UMActivity context,IBinderGroup binderGroup) {
wuserpanel = (XHorizontalLayout)ThirdControl.createControl(new XHorizontalLayout(context),ID_WUSERPANEL
,"margin-left","10"
,"width","fill"
,"valign","center"
,"margin-top","10"
,"background-image","textboxbg.png"
,"layout-type","vbox"
,"halign","LEFT"
,"margin-right","10"
,"height","44"
);
wuserimg = (UMImage)ThirdControl.createControl(new UMImage(context),ID_WUSERIMG
,"src","fa_user.png"
,"width","48"
,"scaletype","fitcenter"
,"layout-type","hbox"
,"height","44"
);
wuserpanel.addView(wuserimg);
wusertext = (UMTextbox)ThirdControl.createControl(new UMTextbox(context),ID_WUSERTEXT
,"color","#a6a6a6"
,"maxlength","256"
,"width","fill"
,"font-size","14"
,"font-family","default"
,"placeholder","手机\\用户名\\邮箱"
,"layout-type","hbox"
,"halign","LEFT"
,"height","44"
);
wuserpanel.addView(wusertext);

return wuserpanel;
}
public View getWpasspanelView(final UMActivity context,IBinderGroup binderGroup) {
wpasspanel = (XHorizontalLayout)ThirdControl.createControl(new XHorizontalLayout(context),ID_WPASSPANEL
,"margin-left","10"
,"width","fill"
,"valign","center"
,"background-image","textboxbg.png"
,"layout-type","vbox"
,"halign","LEFT"
,"margin-right","10"
,"height","44"
);
wpassimg = (UMImage)ThirdControl.createControl(new UMImage(context),ID_WPASSIMG
,"src","fa_password.png"
,"width","48"
,"scaletype","fitcenter"
,"layout-type","hbox"
,"height","44"
);
wpasspanel.addView(wpassimg);
wpasstext = (UMPassword)ThirdControl.createControl(new UMPassword(context),ID_WPASSTEXT
,"color","#a6a6a6"
,"maxlength","256"
,"width","fill"
,"font-size","14"
,"font-family","default"
,"placeholder","密码"
,"layout-type","hbox"
,"halign","LEFT"
,"height","44"
);
wpasspanel.addView(wpasstext);

return wpasspanel;
}
public View getWloginpanelView(final UMActivity context,IBinderGroup binderGroup) {
wloginpanel = (XVerticalLayout)ThirdControl.createControl(new XVerticalLayout(context),ID_WLOGINPANEL
,"width","fill"
,"valign","TOP"
,"background-image","loginpagebg.png"
,"layout-type","vbox"
,"halign","center"
,"onload","action:wloginpanel_onload"
,"height","fill"
);
View wuserpanel = (View) getWuserpanelView((UMActivity)context,binderGroup);
wloginpanel.addView(wuserpanel);
View wpasspanel = (View) getWpasspanelView((UMActivity)context,binderGroup);
wloginpanel.addView(wpasspanel);
wforgetpasspanel = (XVerticalLayout)ThirdControl.createControl(new XVerticalLayout(context),ID_WFORGETPASSPANEL
,"margin-left","10"
,"width","fill"
,"valign","TOP"
,"layout-type","vbox"
,"halign","right"
,"margin-right","10"
,"height","47"
);
wloginpanel.addView(wforgetpasspanel);
wloginbutton = (UMButton)ThirdControl.createControl(new UMButton(context),ID_WLOGINBUTTON
,"font-pressed-color","#e50011"
,"color","#ffffff"
,"onclick","action:wloginbutton_onclick"
,"font-size","20"
,"valign","center"
,"margin-right","10"
,"margin-left","10"
,"background","#f7931e"
,"width","fill"
,"font-family","default"
,"layout-type","vbox"
,"halign","center"
,"value","登录"
,"height","44"
);
wloginpanel.addView(wloginbutton);
wotherpanel = (XHorizontalLayout)ThirdControl.createControl(new XHorizontalLayout(context),ID_WOTHERPANEL
,"margin-left","10"
,"width","fill"
,"valign","top"
,"layout-type","vbox"
,"halign","LEFT"
,"margin-right","10"
,"height","47"
);
wloginpanel.addView(wotherpanel);

return wloginpanel;
}
public View getViewPage0View(final UMActivity context,IBinderGroup binderGroup) {
viewPage0 = (XVerticalLayout)ThirdControl.createControl(new XVerticalLayout(context),ID_VIEWPAGE0
,"background","#F5F5F5"
,"width","fill"
,"valign","TOP"
,"layout-type","vbox"
,"halign","center"
,"height","fill"
);
View navigatorbar0 = (View) getNavigatorbar0View((UMActivity)context,binderGroup);
viewPage0.addView(navigatorbar0);
View wloginpanel = (View) getWloginpanelView((UMActivity)context,binderGroup);
viewPage0.addView(wloginpanel);

return viewPage0;
}
public UMWindow getCurrentWindow(final UMActivity context,IBinderGroup binderGroup) {
Login = (UMWindow)ThirdControl.createControl(new UMWindow(context),ID_LOGIN
,"layout","vbox"
,"controller","LoginController"
,"namespace","com.yonyou.dxyma"
,"width","fill"
,"valign","TOP"
,"onkeydown","action:login_onkeydown"
,"layout-type","linear"
,"halign","center"
,"height","fill"
);
View viewPage0 = (View) getViewPage0View((UMActivity)context,binderGroup);
Login.addView(viewPage0);

return (UMWindow)Login;
}

	
	public void actionButton1_onclick(View control, UMEventArgs args) {
    args.put("containerName","");
    args.put("language","javascript");
    String actionid = "button1_onclick";
  ActionProcessor.exec(this, actionid, args);
  this.getContainer().exec(actionid, "this.button1_onclick()",UMActivity.getViewId(control),args);
}
public void actionLogin_onkeydown(View control, UMEventArgs args) {
    args.put("containerName","");
    args.put("language","javascript");
    String actionid = "login_onkeydown";
  ActionProcessor.exec(this, actionid, args);
  this.getContainer().exec(actionid, "this.onkeydown()",UMActivity.getViewId(control),args);
}
public void actionWloginbutton_onclick(View control, UMEventArgs args) {
    args.put("containerName","");
    args.put("language","javascript");
    String actionid = "wloginbutton_onclick";
  ActionProcessor.exec(this, actionid, args);
  this.getContainer().exec(actionid, "this.wloginbutton_onclick()",UMActivity.getViewId(control),args);
}
public void actionWloginpanel_onload(View control, UMEventArgs args) {
    args.put("containerName","");
    args.put("language","javascript");
    String actionid = "wloginpanel_onload";
  ActionProcessor.exec(this, actionid, args);
  this.getContainer().exec(actionid, "this.wloginpanel_onload()",UMActivity.getViewId(control),args);
}


}
