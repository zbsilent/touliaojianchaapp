package com.yonyou.dxyma;

import com.yonyou.uap.um.application.ApplicationContext;
import com.yonyou.uap.um.base.*;
import com.yonyou.uap.um.common.*;
import com.yonyou.uap.um.third.*;
import com.yonyou.uap.um.control.*;
import com.yonyou.uap.um.core.*;
import com.yonyou.uap.um.binder.*;
import com.yonyou.uap.um.runtime.*;
import com.yonyou.uap.um.lexer.*;
import com.yonyou.uap.um.widget.*;
import com.yonyou.uap.um.widget.UmpSlidingLayout.SlidingViewType;
import com.yonyou.uap.um.log.ULog;
import java.util.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import android.webkit.*;
import android.content.*;
import android.graphics.*;
import android.widget.ImageView.ScaleType;

public abstract class MachineActivity extends UMWindowActivity {

	protected UMWindow Machine = null;
protected XVerticalLayout viewPage0 = null;
protected XHorizontalLayout navigatorbar0 = null;
protected UMButton button0 = null;
protected UMLabel label0 = null;
protected UMCustomPickerLayout picker0 = null;
protected UMCustomPickerItem picker0_0 = null;
protected UMButton button2 = null;

	
	protected final static int ID_MACHINE = 703775073;
protected final static int ID_VIEWPAGE0 = 1899362136;
protected final static int ID_NAVIGATORBAR0 = 2015280677;
protected final static int ID_BUTTON0 = 1704662124;
protected final static int ID_LABEL0 = 10732707;
protected final static int ID_PICKER0 = 207614610;
protected final static int ID_PICKER0_0 = 1076895534;
protected final static int ID_BUTTON2 = 1637568284;

	
	
	@Override
	public String getControllerName() {
		return "MachineController";
	}
	
	@Override
	public String getContextName() {
		return "";
	}

	@Override
	public String getNameSpace() {
		return "com.yonyou.dxyma";
	}

	protected void onCreate(Bundle savedInstanceState) {
		ULog.logLC("onCreate", this);
		super.onCreate(savedInstanceState);
        onInit(savedInstanceState);
        
	}
	
	@Override
	protected void onStart() {
		ULog.logLC("onStart", this);
		
		super.onStart();
	}

	@Override
	protected void onRestart() {
		ULog.logLC("onRestart", this);
		
		super.onRestart();
	}

	@Override
	protected void onResume() {
		ULog.logLC("onResume", this);
		
		super.onResume();
	}

	@Override
	protected void onPause() {
		ULog.logLC("onPause", this);
		
		super.onPause();
	}

	@Override
	protected void onStop() {
		ULog.logLC("onStop", this);
		
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		ULog.logLC("onDestroy", this);
		
		super.onDestroy();
	}
	
	public  void onInit(Bundle savedInstanceState) {
		ULog.logLC("onInit", this);
		UMActivity context = this;
		registerControl();
		this.getContainer();
		
		/*
		 new Thread() {
			 public void run() {
				 UMPDebugClient.startServer();
			 }
		 }.start();
		*/
		String sys_debug = ApplicationContext.getCurrent(this).getValue("sys_debug");
		if (sys_debug != null && sys_debug.equalsIgnoreCase("true")) {
			UMPDebugClient.waitForDebug();
		}

		IBinderGroup binderGroup = this;
		currentPage = getCurrentWindow(context, binderGroup);
super.setContentView(currentPage);

		
	}
	
	private void registerControl() {
		  idmap.put("Machine",ID_MACHINE);
  idmap.put("viewPage0",ID_VIEWPAGE0);
  idmap.put("navigatorbar0",ID_NAVIGATORBAR0);
  idmap.put("button0",ID_BUTTON0);
  idmap.put("label0",ID_LABEL0);
  idmap.put("picker0",ID_PICKER0);
  idmap.put("picker0_0",ID_PICKER0_0);
  idmap.put("button2",ID_BUTTON2);

	}
	
	public void onLoad() {
		ULog.logLC("onLoad", this);
		if(currentPage!=null) {
			currentPage.onLoad();
		}
	
		{ //viewPage0 - action:viewpage0_onload
    UMEventArgs args = new UMEventArgs(MachineActivity.this);
    actionViewpage0_onload(viewPage0,args);

}
{ //picker0 - action:picker0_onload
    UMEventArgs args = new UMEventArgs(MachineActivity.this);
    actionPicker0_onload(picker0,args);

}

	}
	
	public void onDatabinding() {
		ULog.logLC("onDatabinding", this);
		super.onDatabinding();
		
	}
	
	@Override
	public void onReturn(String methodName, Object returnValue) {
		
	}

	@Override
	public void onAfterInit() {
		ULog.logLC("onAfterInit", this);
		
		onLoad();
	}
	
		@Override
	public Map<String,String> getPlugout(String id) {
		XJSON from = this.getUMContext();
		Map<String,String> r = super.getPlugout(id);
		
		return r;	
	}
	
	
	
	public View getNavigatorbar0View(final UMActivity context,IBinderGroup binderGroup) {
navigatorbar0 = (XHorizontalLayout)ThirdControl.createControl(new XHorizontalLayout(context),ID_NAVIGATORBAR0
,"color","#323232"
,"padding-left","8"
,"font-size","17"
,"valign","center"
,"title","机台号"
,"width","fill"
,"font-family","default"
,"background-image","navbar_login.png"
,"layout-type","vbox"
,"halign","LEFT"
,"padding-right","8"
,"height","44.0"
);
button0 = (UMButton)ThirdControl.createControl(new UMButton(context),ID_BUTTON0
,"font-pressed-color","#e50011"
,"color","#ffffff"
,"width","44"
,"font-size","20"
,"font-family","default"
,"valign","center"
,"background-image","icon_back.png"
,"layout-type","hbox"
,"halign","center"
,"height","44"
);
navigatorbar0.addView(button0);
label0 = (UMLabel)ThirdControl.createControl(new UMLabel(context),ID_LABEL0
,"width","0"
,"weight","1"
,"valign","center"
,"font-family","default"
,"layout-type","hbox"
,"halign","center"
,"height","fill"
);
navigatorbar0.addView(label0);

return navigatorbar0;
}
public View getPicker0View(final UMActivity context,IBinderGroup binderGroup) {
picker0 = (UMCustomPickerLayout)ThirdControl.createControl(new UMCustomPickerLayout(context),ID_PICKER0
,"width","fill"
,"margin-top","100"
,"layout-type","vbox"
,"onload","action:picker0_onload"
,"height","186"
);
picker0_0 = (UMCustomPickerItem)ThirdControl.createControl(new UMCustomPickerItem(context),ID_PICKER0_0
,"bindfield","machine_code"
,"datasource","machine_codes"
,"onselectedchange","action:picker0_0_onselectedchange"
,"layout-type","vbox"
,"value","机台"
);
picker0.addView(picker0_0);

return picker0;
}
public View getViewPage0View(final UMActivity context,IBinderGroup binderGroup) {
viewPage0 = (XVerticalLayout)ThirdControl.createControl(new XVerticalLayout(context),ID_VIEWPAGE0
,"background","#F5F5F5"
,"width","fill"
,"valign","TOP"
,"layout-type","vbox"
,"halign","center"
,"onload","action:viewpage0_onload"
,"height","fill"
);
View navigatorbar0 = (View) getNavigatorbar0View((UMActivity)context,binderGroup);
viewPage0.addView(navigatorbar0);
View picker0 = (View) getPicker0View((UMActivity)context,binderGroup);
viewPage0.addView(picker0);
button2 = (UMButton)ThirdControl.createControl(new UMButton(context),ID_BUTTON2
,"font-pressed-color","#f2adb2"
,"color","#ffffff"
,"onclick","action:button2_onclick"
,"font-size","17"
,"valign","center"
,"margin-right","50"
,"margin-left","50"
,"background","#ff8000"
,"width","fill"
,"font-family","default"
,"margin-top","50"
,"layout-type","vbox"
,"halign","center"
,"value","登录"
,"height","44"
);
viewPage0.addView(button2);

return viewPage0;
}
public UMWindow getCurrentWindow(final UMActivity context,IBinderGroup binderGroup) {
Machine = (UMWindow)ThirdControl.createControl(new UMWindow(context),ID_MACHINE
,"layout","vbox"
,"controller","MachineController"
,"namespace","com.yonyou.dxyma"
,"width","fill"
,"valign","TOP"
,"layout-type","linear"
,"halign","center"
,"height","fill"
);
View viewPage0 = (View) getViewPage0View((UMActivity)context,binderGroup);
Machine.addView(viewPage0);

return (UMWindow)Machine;
}

	
	public void actionPicker0_onload(View control, UMEventArgs args) {
    args.put("containerName","");
    args.put("language","javascript");
    String actionid = "picker0_onload";
  ActionProcessor.exec(this, actionid, args);
  this.getContainer().exec(actionid, "this.picker0_onload()",UMActivity.getViewId(control),args);
}
public void actionButton2_onclick(View control, UMEventArgs args) {
    args.put("containerName","");
    args.put("language","javascript");
    String actionid = "button2_onclick";
  ActionProcessor.exec(this, actionid, args);
  this.getContainer().exec(actionid, "this.button2_onclick()",UMActivity.getViewId(control),args);
}
public void actionViewpage0_onload(View control, UMEventArgs args) {
    args.put("containerName","");
    args.put("language","javascript");
    String actionid = "viewpage0_onload";
  ActionProcessor.exec(this, actionid, args);
  this.getContainer().exec(actionid, "this.pageOnLoad()",UMActivity.getViewId(control),args);
}
public void actionPicker0_0_onselectedchange(View control, UMEventArgs args) {
    args.put("containerName","");
    args.put("language","javascript");
    String actionid = "picker0_0_onselectedchange";
  ActionProcessor.exec(this, actionid, args);
  this.getContainer().exec(actionid, "onChange()",UMActivity.getViewId(control),args);
}


}
