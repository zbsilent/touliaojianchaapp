package com.yonyou.mes;

import java.util.HashMap;
import java.util.Map;
import com.yonyou.uap.um.controller.AbstractUMController;
import com.yonyou.uap.um.gateway.service.GatewayServiceFactory;
import com.yonyou.uap.um.gateway.service.IGatewayService;
import com.yonyou.uap.um.gateway.xml.GatewayNodeFactory;
import com.yonyou.uap.um.gateway.xml.IServiceNode;
import com.yyuap.upush.common.json.JSONObject;

public class HttpCaller extends AbstractUMController {

	/**
	 * 调MA服务
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public String call(String param) throws Exception{
		JSONObject allobj=new JSONObject(param);
		JSONObject myparam=allobj.getJSONObject("myparam");
		String ncusercode=ConfigReader.getNCUsercode();
		String ncpassword=ConfigReader.getNCUserPassword();
		String ncdatasource=ConfigReader.getNCDatasource();
		myparam.put("sysparam_usercode", ncusercode);
		myparam.put("sysparam_password", ncpassword);
		myparam.put("sysparam_datasource", ncdatasource);
		Map paramMap=new HashMap();
		paramMap.put("params", myparam.toString());
		IServiceNode svr = GatewayNodeFactory.getGatewayNode("dxyma").getServiceNode("mesApp2NCService");
		IGatewayService service = GatewayServiceFactory.findGatewayService(svr,paramMap);
		String result=null;
		if (service != null) {
			result =(String)service.doService();
		}
		return result;
	}
	
	//测试服务器连接
	public String callTest(String param) throws Exception{
		String result = "MA_success";
		return result;
	}
	
	@Override
	public String load(String arg0) throws Exception {
		// TODO 自动生成的方法存根
		return null;
	}

	@Override
	public String save(String arg0) throws Exception {
		// TODO 自动生成的方法存根
		return null;
	}

}
