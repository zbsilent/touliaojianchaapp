package com.yonyou.mes;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import com.ufida.iufo.pub.tools.MD5;

public class ConfigReader {
	private static Map<String,String> properties=new HashMap<String,String>();
	public static String getNCDatasource(){
		return getPropValue("ncdatasource");
	}
	public static String getNCUsercode(){
		return getPropValue("ncusercode");
	}
	public static String getNCUserPassword(){
		return getPropValue("ncuserpassword");
	}
	private static void readConfigFile(){
		File nowfile=new File("");
		String abspath=nowfile.getAbsolutePath();
		File file = new File(abspath+"\\conf\\dxymescfg.txt");
		Properties props = new Properties();
		FileInputStream fis;
		try {
			fis = new FileInputStream(file);
			props.load(new InputStreamReader(fis,"UTF-8"));//将属性文件流装载到Properties对象中   
			Set<String> pnames=props.stringPropertyNames();
			if(pnames!=null&&pnames.size()>0){
				for(String pname:pnames){
					properties.put(pname, props.getProperty(pname));
				}
			}
		} catch (Exception e) {
			ExceptionUtils.dealException(e);
		}
	}
	private static String getPropValue(String pname){
		if(pname==null){
			return null;
		}
		//支持重新读取配置文件
		if(!properties.containsKey(pname)){
			readConfigFile();
		}
		if(properties.containsKey(pname)){
			return properties.get(pname);
		}else{
			return null;
		}
	}
}

